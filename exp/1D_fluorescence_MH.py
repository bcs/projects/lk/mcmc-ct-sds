"""
Gene switching data from CW
"""
import pickle
import numpy as np
import os
import torch
import scipy.stats as st
import sklearn.cluster
import src.model as model
from datetime import datetime
from src.utils.paths import DATA_PATH

EPS = 1e-10  # prevents numerical issues such as log(0) = nan

# Reproducibility
np.random.seed(37)
torch.manual_seed(37)

experiment_name = __file__[__file__.rfind('/') + 1: -6]  # extract the filename between last `/' and the `.npy', i.e., the experiment's name

data = np.load('../' + DATA_PATH + experiment_name + '/trace_5.npy')
t_obs = data[:, 0]
X_obs = data[:, 1]
X_obs = X_obs[None, :]
time_scaling = 10./t_obs[-1]
t_obs *= time_scaling
#t_span = [0.0, 0.4]
t_span = [0.0, 10.] # Scale for numerical stability

t_obs = t_obs[1:-1]  # throw away observations at boundaries
empirical_mean = np.mean(X_obs[0, :])
empirical_std = np.std(X_obs[0, :])
X_obs = (X_obs[:, 1:-1] - empirical_mean)/empirical_std  # rescale y-axis to prevent numerical issues
data = X_obs
################################################################################
# Perform inference
n_states = 2
n_dim = 1
kmeans = sklearn.cluster.k_means(X_obs.reshape(-1, 1), n_clusters=n_states)

# For initialization, perform k-means
empirical_means = []
clusters = []
for i in range(n_states):
    clusters.append(X_obs[0, np.where(kmeans[1] == int(i))[0]])
    empirical_means.append([np.mean(clusters[-1])])

empirical_means = [[X_obs[0,0]], [np.mean(X_obs[0, np.where(kmeans[1]==1.)[0]])]]

# Find switching points in kmeans; switching_indices is an array holding the indices of the LAST time point beloning to the preceding mode
slopes = [[], []]
switching_indices = np.where(np.diff(kmeans[1]) != 0)[0]
for i in range(switching_indices.shape[0] + 1):
    if i == 0:
        dx = np.diff(X_obs[:, 0:switching_indices[i]+1])
        dt = np.diff(t_obs[0:switching_indices[i]+1])
        slopes[kmeans[1][switching_indices[i]]].append(dx/dt)
    elif i < switching_indices.shape[0]:
        dx = np.diff(X_obs[:, switching_indices[i-1]:switching_indices[i]])
        dt = np.diff(t_obs[switching_indices[i-1]:switching_indices[i]])
        slopes[kmeans[1][switching_indices[i]]].append(dx/dt)
    else:
        dx = np.diff(X_obs[:, switching_indices[i - 1]:])
        dt = np.diff(t_obs[switching_indices[i - 1]:])
        slopes[kmeans[1][switching_indices[i-1]+1]].append(dx / dt)

empirical_slope = np.zeros((n_states, n_dim))
for i in range(n_states):
    empirical_slope[i] = -1 * np.abs(np.mean(np.concatenate(slopes[i], axis=1)))

# Empirical covariance \approx empirical quadratic variation
dx = np.diff(X_obs[0])
dt = np.diff(t_obs)
empirical_cov = np.mean(dx ** 2 / dt)
empirical_cov = empirical_cov[None, None] * 0.1 
empirical_covs = np.array([empirical_cov for i in range(n_states)])
#empirical_slope = np.abs((dx/dt).mean())

# For init:
cov = np.cov(X_obs)[None, None]

# Define hyperparameters of prior distributions
empirical_init_mjp = np.zeros(n_states)
empirical_init_mjp[kmeans[1][0]] += 10
empirical_mjp_transitions = np.where(np.diff(kmeans[1]) != 0)[0].shape[0]/(t_span[1]-t_span[0])
mjp_init_pseudocounts = np.ones(n_states) + empirical_init_mjp
mjp_gamma_pseudocounts = dict(shape=empirical_mjp_transitions * np.ones((n_states, n_states)),
                              rate=1* np.ones((n_states, n_states)))
sde_init_parameters = dict(N_location=empirical_means[kmeans[1][0]],
                           N_scale=1,
                           IW_dof=n_dim + 2,
                           IW_scale_mtx=np.mean(empirical_covs, axis=0))
sde_mn_parameters = dict(MN_location=empirical_slope[:, :, None] * np.ones((n_states, n_dim, n_dim + 1)),
                         MN_scale_mtx=1 * np.array([np.eye(n_dim + 1) for n in range(n_states)]),
                         IW_dof=n_dim + 2,
                         IW_scale_mtx=torch.tensor(np.array(empirical_covs), dtype=torch.float64))
sde_mn_parameters['MN_location'][:, :, -1] = (
        -1 * np.mean(sde_mn_parameters['MN_location'][:, :, :-1], axis=0) @ np.array(empirical_means)[..., None]).squeeze(-1)
obs_parameters = dict(IW_dof=n_dim + 2,
                      IW_scale_mtx=np.mean(empirical_covs, axis=0) * 0.5)

# Draw initial parameters from priors
initial_mjp_dist = np.ones(n_states) * EPS
initial_mjp_dist[kmeans[1][0]] = 1. - EPS
initial_sde_mean = np.array(empirical_means[kmeans[1][0]])
initial_sde_cov = np.array(empirical_covs[kmeans[1][0]])

# Initialize SDE parameters empirically
sde_cov = np.zeros((n_states, n_dim, n_dim))
sde_drift_intercept = np.zeros((n_states, n_dim))
sde_drift_slope = np.zeros((n_states, n_dim, n_dim))
for z in range(n_states):
    sde_cov[z] = np.array(empirical_covs)[z]
    sde_drift_slope[z] = -1 * np.eye(n_dim)
    sde_drift_intercept[z] = -1 * sde_drift_slope[z] @ np.array(empirical_means)[z][..., None].squeeze(-1)

# Draw MJP parameters from priors
Lambda = np.zeros((n_states, n_states))
for z_from in range(n_states):
    for z_to in range(n_states):
        if z_to != z_from:
            Lambda[z_from, z_to] = 1/(n_states - 1)

# Make Lambda a proper Q-matrix
diag = np.einsum('ii->i', Lambda)
diag[:] = -1 * np.sum(Lambda, axis=1)
rate_mtx = Lambda

# Set observation covariance
obs_cov = torch.tensor([[0.08]])
M = model.PosteriorHybridSystemMH(t_span=t_span, t_obs=t_obs, obs_data=X_obs,
                                     rate_mtx=rate_mtx,
                                     initial_posterior_mjp=initial_mjp_dist,
                                     initial_posterior_mean=initial_sde_mean,
                                     initial_posterior_cov=initial_sde_cov,
                                     diffusion_slope=sde_drift_slope,
                                     diffusion_intercept=sde_drift_intercept,
                                     diffusion_cov=sde_cov,
                                     obs_cov=obs_cov,
                                     mjp_init_pseudocounts=mjp_init_pseudocounts,
                                     mjp_gamma_pseudocounts=mjp_gamma_pseudocounts,
                                     sde_init_parameters=sde_init_parameters,
                                     sde_mn_parameters=sde_mn_parameters,
                                     obs_parameters=obs_parameters,
                                     verbose=True)

# Initialize Z-path with k-means-path
states = kmeans[1]
jump_indices = np.where(np.diff(states) != 0)[0]
jump_times = t_obs[jump_indices]
mjp_path = np.array(list(states[jump_indices]) + [states[-1]])
M.jump_times = jump_times
M.mjp_path = mjp_path

# Sampler hyperparameters
N_samples = 100000
checkpoint_interval = 20000
dt = 1e-2  # Euler-Maruyama step size
opts_sample_params = dict(sample_initial_conditions=True,
                          sample_sde_drift_parameters=True,
                          update_sde_dispersion_parameters=True,
                          sample_mjp_parameters=True,
                          sample_observation_parameters=False)

# Timestamp for checkpoints, save before starting Gibbs sampler
timestamp = datetime.now().strftime('%Y%m%d-%H%M%S')
with open(__file__, 'r') as f:
    with open('../' + DATA_PATH + experiment_name + f'/experiment_script_copy_{timestamp}', 'w') as out:
        for line in (f.readlines()[:-7]):  # remove last 7 lines
            print(line, end='', file=out)

# Finally: sample
M.fit(N_samples, dt,
      sample_parameters=True,
      checkpoint_name=experiment_name,
      checkpoint_interval=checkpoint_interval,
      opts_sample_params=opts_sample_params)
