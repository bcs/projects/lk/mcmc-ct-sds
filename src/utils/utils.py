import logging

import numpy as np
import scipy.integrate as integrate
from numba import jit
from scipy.integrate import OdeSolution
from scipy.integrate._ivp.base import ConstantDenseOutput
from scipy.integrate._ivp.ivp import OdeSolution
from typing import List, Union, Tuple, Iterable, Any
import torch


def cat_ode_solutions(ode_solution_list: List[OdeSolution], shape: Union[int, Tuple[int, ...]] = None,
                      order: str = 'C') -> OdeSolution:
    """
    Concatenates a list of ordered piecewise Ode Solutions and returns one single (Reshaped)OdeSolution object

    :param ode_solution_list: list of the ode solutions
    :param shape: reshape tuple for the evaluated ode solution at time point t --> see np.reshape
    :param order: see np.reshape
    :return: concatenated ode solution
    """
    ts_list = []
    interpolants = []
    for ode_solution in ode_solution_list:
        ts = ode_solution.ts[:-1]
        ts_list.append(ts)
        interpolants += ode_solution.interpolants

    ts = np.append(np.concatenate(ts_list), ode_solution_list[-1].ts[-1])
    ode_solution = OdeSolution(ts, interpolants)
    if shape is not None:
        ode_solution = ReshapedOdeSolution(ode_solution, shape, order=order)

    return ode_solution


class ReshapedOdeSolution(OdeSolution):
    def __init__(self, ode_solution: OdeSolution, shape: Union[int, Tuple[int, ...]], order: str = 'C',
                 sub_idxs: List[int] = None):
        """
        Reshapes OdeSolution on each call at time point t

        :param ode_solution: OdeSolution to be reshaped
        :param shape: new shape --> see np.reshape
        :param order: --> see np.reshape
        :param sub_idxs: sub indices for which the solution is called ode_solution(t)[sub_idxs[0]:sub_idxs[1]] (default all)
        """
        if sub_idxs is None:
            sub_idxs = [0, ode_solution(0.0).size]

        self.sub_idxs = sub_idxs
        self.shape = shape
        self.order = order
        OdeSolution.__init__(self, ts=ode_solution.ts, interpolants=ode_solution.interpolants)

    def __call__(self, t: Any):
        """
        Modifies call function to return reshaped value for base function see OdeSolution.__call__
        """
        t = np.asarray(t)
        res = OdeSolution.__call__(self, t)

        if t.ndim == 0:
            res = res[self.sub_idxs[0]:self.sub_idxs[1]].reshape(self.shape, order=self.order)
        else:
            newshape = self.shape
            if isinstance(newshape, int):
                newshape = (newshape,)
            res = res[self.sub_idxs[0]:self.sub_idxs[1], :].reshape(newshape + (t.size,), order=self.order)

        return res


def get_constant_function(t_span: Union[np.ndarray, Iterable, int, float], const: Any, shape: Any):
    """
    Creates a constant function in the interval t_span with constant const and reshaped according to shape
    :param t_span: time interval
    :param const: constant (can be flatened array)
    :param shape: new shape the solution should have at each time point t
    :return:
    """
    flat_ode_solution = OdeSolution(t_span, [ConstantDenseOutput(*t_span, const * np.ones(np.prod(shape)))])
    return ReshapedOdeSolution(flat_ode_solution, shape=shape)


def discrete_kl(qk: torch.Tensor, pk: torch.Tensor, dim=0) -> torch.Tensor:
    """
    Computes the KL divergence beteween two discrete distributions KL(q||p)
    :param qk: [n_states] torch tensor for the probabilities of the first distribution
    :param pk: [n_states] torch tensor for the probabilities of the second distribution
    :param dim: dimension index to sum over
    :return: kl divergence  KL(q||p)
    """
    return (qk * (qk.log() - pk.log())).sum(dim=dim)


def gaussian_kl(mu_q: torch.Tensor, Sigma_q: torch.Tensor, mu_p: torch.Tensor, Sigma_p: torch.Tensor):
    """
    Computes the KL between two gaussians KL(N(mu_q,Sigma_q) || N(mu_p,Sigma_p))

    :param mu_q: [batch 1, batch 2,..., n_states] mean of the posterior Gaussian
    :param Sigma_q: [batch 1, batch 2,...,n_states, n_states] covariance of the posterior Gaussian
    :param mu_p: [batch 1, batch 2,..., n_states] mean of the posterior Gaussian
    :param Sigma_p: [batch 1, batch 2,...,n_states, n_states] covariance of the posterior Gaussian
    :return: (scalar) KL divergence
    """
    n = mu_q.shape[-1]
    mu_diff = mu_p[..., None] - mu_q[..., None]  # Dimension batch 1 x batch 2 x... xNxN'
    mu_diff_T = torch.transpose(mu_diff, -2, -1)  # Dimension batch 1 x batch 2 x... xN'xN
    Sigma_prior_inv = torch.inverse(Sigma_p)  # Dimension batch 1 x batch 2 x... xNxN

    # Compute KL(q(y|z) || p(y|z))]
    # =1/2 ( log(det(Sigma_prior)/det(Sigma_post))
    #   +tr(Sigma_prior^-1 Sigma_post)
    #   +(mu_prior-mu_post)^T Sigma_prior^-1 (mu_prior-mu_post)
    #   - n )

    kl = torch.logdet(Sigma_p) - torch.logdet(Sigma_q)  # Dimension batch 1 x batch 2 x... batch n
    kl += torch.sum(Sigma_prior_inv * Sigma_q, dim=(-2, -1))  # Dimension batch 1 x batch 2 x... batch n
    kl += quad_form(mu_diff, Sigma_prior_inv)  # Dimension batch 1 x batch 2 x... batch n
    kl -= n  # Dimension batch 1 x batch 2 x... batch n (broadcasting)

    return .5 * kl


def quad_form(A: Union[torch.Tensor, np.ndarray], B: Union[torch.Tensor, np.ndarray],
              C: Union[torch.Tensor, np.ndarray] = None, keepdims: bool = False):
    """
    Computes the quadratic form A_T @ B @ C batchwise

    :param A: [batch 1 x batch 2 x ...x N (x N)] or [N (x N)] vector or matrix
    :param B: [batch 1 x batch 2 x ...x N x N] or [N x N] matrix
    :param C: [batch 1 x batch 2 x ...x N (x N)] or [N (x N)] vector or matrix if None A is used
    :param keep dimensions when performing product
    :return: scalar, [batch 1 x batch 2 x ...] or [NxN], [batch 1 x batch 2 x ...xNxN] output depending on keepdims
    """
    if A.ndim != B.ndim:
        A = A[..., None]

    if C is None:
        C = A
    else:
        if C.ndim != B.ndim:
            C = C[..., None]

    if isinstance(A, np.ndarray):
        out = np.moveaxis(A, -2, -1) @ B @ C
    elif isinstance(A, torch.Tensor):
        out = torch.transpose(A, -2, -1) @ B @ C
    else:
        raise TypeError

    if keepdims:
        return out
    else:
        return out.squeeze(-1).squeeze(-1)


def quadrature_intervals(integrand: Any, interval_boundaries: Iterable, **kwargs) -> Tuple[float,float]:
    """
    Returns the integral of the given scalar-valued integrand over the time span [interval_boundaries[0], interval_boundaries[-1]],
    where the integration is carried out piecewise over intervals [interval_boundaries[i], interval_boundaries[i+1]].

    :param integrand: function object
    :param interval_boundaries: iterable holding the interval boundaries for integration
    :param kwargs parameters passed to scipy.integrate.quadrature

    Returns
    -------
    val : float
        Gaussian quadrature approximation (within tolerance) to integral.
    err : float
        Difference between last two estimates of the integral.
    """
    val_cum = 0.0
    err_cum = 0.0
    for _interval in zip(interval_boundaries[:-1], interval_boundaries[1:]):
        val, err = integrate.quadrature(integrand, *_interval, **kwargs)
        val_cum += val
        err_cum += err
    return val_cum, err_cum


def quad_vec_intervals(integrand: Any, interval_boundaries: Iterable, **kwargs) -> Tuple[Any,float]:
    """
    Returns the integral of the given vector-valued integrand over the time span
     [interval_boundaries[0], interval_boundaries[-1]], where the integration is carried out piecewise over intervals
     [interval_boundaries[i], interval_boundaries[i+1]].

    :param integrand: function object
    :param interval_boundaries: iterable holding the interval boundaries for integration
    :param kwargs parameters passed to scipy.integrate.quad_vec

    Returns
    -------
    res : {float, array-like}
        Estimate for the result
    err : float
        Error estimate for the result in the given norm
    """
    val_cum = 0.0
    err_cum = 0.0
    for _interval in zip(interval_boundaries[:-1], interval_boundaries[1:]):
        val, err = integrate.quad_vec(integrand, *_interval, **kwargs)[:2]
        val_cum += val
        err_cum += err

    return val_cum, err_cum


@jit(nopython=True)
def jit_sample_posterior_mjp_reverse(t_span, final_belief, posterior_rate_mtx, posterior_rate_mtx_time_grid):
    n_states = final_belief.shape[0]
    n_time_points = posterior_rate_mtx_time_grid.shape[0]
    trajectory = []
    jump_times = []
    t_start, t_end = t_span[::-1]
    assert t_start > t_end
    t_query_rate = posterior_rate_mtx_time_grid

    p = (final_belief / np.sum(final_belief))[:-1]
    u = np.random.rand()
    z_init = np.searchsorted(p, u, side='right')
    trajectory.append(z_init)

    # 1. Set global upper bound (overall maxmimum rate from the current time point onwards) TODO make smarter/more efficient
    future_exit_rates = np.zeros((n_states, n_time_points))
    for n in range(n_states):
        future_exit_rates[n, :] = posterior_rate_mtx[n, n, :]
    lambda_star = np.amax(np.abs(future_exit_rates))

    t_cur = t_start
    while t_cur > t_end:
        # 2. Sample inter-arrival time
        u = np.random.rand()
        tau = -1 * np.log(u) / lambda_star

        # 3. Update current time
        t_cur -= tau
        if t_cur <= t_end:
            break

        t_cur_idx = np.searchsorted(t_query_rate, t_cur)

        # Get instantaneous rate at new time
        rate_mtx_cur = posterior_rate_mtx[:, :, t_cur_idx]  # Dimension ZxZ'
        exit_rates = np.diag(rate_mtx_cur)

        lambda_t = np.abs(exit_rates[trajectory[-1]])

        # Compare instantaneous to maximum rate and accept the jump time with probability lambda_t/lambda_star
        s = np.random.rand()
        if s <= lambda_t / lambda_star:
            jump_times.append(t_cur)

            # find next state
            transition_probabilities = ((np.eye(n_states) + rate_mtx_cur / lambda_t)[trajectory[-1]])[:-1]
            u = np.random.rand()
            z_next = np.searchsorted(transition_probabilities, u, side='right')
            trajectory.append(z_next)

    return jump_times[::-1], trajectory[::-1]


@jit(nopython=True)
def jit_euler_maruyama(y_init, slope, intercept, dispersion, dW, N_timesteps, dt):
    n_dim = y_init.shape[0]
    res = np.zeros((n_dim, N_timesteps))

    res[:, 0] = y_init

    for i in range(1, N_timesteps):
        y_cur = res[:, i - 1]
        y_next = (y_cur + (slope[i - 1] @ y_cur + intercept[i - 1]) * dt + dispersion[i - 1] @ dW[i])
        res[:, i] = y_next

    return res


@jit(nopython=True)
def jit_wonham(belief_init, slope, intercept, rate_mtx, cov_inv, otraj, dt, eps):
    N_timesteps = otraj.shape[1]
    N_states = belief_init.shape[0]
    N_dim = cov_inv.shape[-1]
    res = np.zeros((N_states, N_timesteps))
    res[:, 0] = belief_init

    dotraj = np.diff(otraj)
    for i in range(1, N_timesteps):
        belief = res[:, i - 1]
        o = otraj[:, i - 1]
        do = dotraj[:, i - 1]
        b_prior_inc = (rate_mtx.T @ belief) * dt  # Dimension Z

        prior_drift = np.zeros((N_states, N_dim, 1))
        g_bar_tmp = np.zeros((N_states, N_dim))
        for z in range(N_states):
            drift = slope[z] @ o + intercept[z]
            prior_drift[z, :, 0] = drift  # Dim Z x N x 1
            g_bar_tmp[z, :] = drift * belief[z]

        g_bar = g_bar_tmp.sum(0)
        ### Kushner-Stratonovich
        db = np.zeros(N_states)
        for z in range(N_states):
            db[z] = b_prior_inc[z] + belief[z] * ((prior_drift[z] - g_bar).T @ cov_inv[z] @ (do - g_bar * dt))[0]

        belief_cur = res[:, i - 1] + db
        belief_cur[belief_cur < eps] = eps
        belief_cur /= belief_cur.sum()

        res[:, i] = belief_cur

    return res