"""
Module holding a class used for (forward) simulation of a continuous-discrete Hybrid system.
"""
import logging
import numpy as np
import torch
import scipy.integrate as integrate
import scipy.interpolate as interpolate
import scipy.stats as st
from datetime import datetime
from copy import deepcopy
from src.utils.utils import cat_ode_solutions, ReshapedOdeSolution, jit_sample_posterior_mjp_reverse, \
    jit_euler_maruyama, jit_wonham
from src.utils.paths import CHECKPOINT_PATH
from torch import nn
from torch.distributions import MultivariateNormal
from torch.optim import Optimizer, Adam
from typing import List, Union, Tuple, Any, Callable, Dict

EPS = 1e-8


class PosteriorHybridSystemMH(nn.Module):

    def __init__(self, t_span: Union[List, np.ndarray],
                 t_obs: Union[np.ndarray, torch.Tensor],
                 obs_data: Union[np.ndarray, torch.Tensor],
                 rate_mtx: Union[np.ndarray, torch.Tensor],
                 initial_posterior_mjp: Union[np.ndarray, torch.Tensor],
                 diffusion_slope: Union[np.ndarray, torch.Tensor],
                 diffusion_intercept: Union[np.ndarray, torch.Tensor],
                 diffusion_cov: Union[np.ndarray, torch.Tensor],
                 obs_cov: Union[np.ndarray, torch.Tensor],
                 initial_posterior_mean: Union[np.ndarray, torch.Tensor],
                 initial_posterior_cov: Union[np.ndarray, torch.Tensor],
                 mjp_init_pseudocounts: Union[np.ndarray, torch.Tensor],
                 mjp_gamma_pseudocounts: Dict,
                 sde_init_parameters: Dict,
                 sde_mn_parameters: Dict,
                 obs_parameters: Dict,
                 verbose=False,
                 Optimizer_constructor: Optimizer = None,
                 optimizer_opts: Dict = None
                 ):
        """
        Class implementing the fully Bayesian SHS sampling framework.

        Internal working structure is as follows:
            0. Setters and Getters
            1. MJP Inference
                1.1.X Forward filtering - Backward smoothing/sampling methods (Anderson)
                1.2.X Backward filtering - Forward smoothing/sampling methods (van Handel)
            2. SDE Inference
                2.1.X Forward filtering - Backward smoothing/sampling (Kalman/RTS)
                2.2.X Backward filtering - Forward smoothing/sampling (van der Meulen)
            3. Gibbs Sampler

        :param t_span: [2,] iterable,  the total time interval
        :param t_obs: [N,] tensor, observation time points
        :param obs_data: [n_dim, N] tensor, obs_data[:, i] holds observation vector obtained at t_obs[i]
        :param rate_mtx: [n_states, n_states] tensor, the MJP rate matrix
        :param initial_posterior_mjp: [n_states] tensor, the MJP inital distribution of the prior
        :param diffusion_slope: [n_states, n_dim, n_dim] tensor, prior slope
        :param diffusion_intercept: [n_states, n_dim] tensor, prior intercept
        :param diffusion_cov: [n_states, n_dim, n_dim] tensor, prior diffusion covariance
        :param obs_cov: [n_dim, n_dim] tensor, the covariance matrix of the observation process
        :param initial_posterior_mean: [n_states,n_dim]  tensor, the diffusion mean initial value of the posterior (start value)
        :param initial_posterior_cov: [n_states,n_dim,ndim]  tensor, the diffusion mean initial value of the posterior (start value)
        :param verbose: boolean flag for logging
        """
        if verbose:
            logging.basicConfig(level=logging.DEBUG)
        else:
            logging.basicConfig(level=logging.INFO)

        super().__init__()

        # Register Z-process (MJP) parameters
        self.n_states = None
        self.register_buffer('_rate_mtx', None)
        self.register_buffer('rate_mtx_zerodiag', None)
        self.register_buffer('_initial_posterior_mjp_params', None)  # optimizable parameters
        self.posterior_rate_mtx = None
        self.mjp_init_pseudocounts = mjp_init_pseudocounts
        self.mjp_gamma_pseudocounts = mjp_gamma_pseudocounts

        # Register Y-process (SDE) parameters
        self.n_dim = None
        self.register_buffer('_diffusion_slope', None)
        self.register_buffer('_diffusion_intercept', None)
        self.register_parameter('_diffusion_cov_chol', None)
        self.register_buffer('_diffusion_cov_inv', None)
        self.register_buffer('initial_posterior_mean', None)
        self.register_buffer('initial_posterior_cov', None)
        self.register_buffer('_initial_posterior_cov_chol', None)
        self.register_buffer('_diffusion_cov_chol_q', None)
        self.register_buffer('_diffusion_cov_inv_q', None)

        self.sde_init_parameters = sde_init_parameters
        self.sde_mn_parameters = sde_mn_parameters

        # Register X-process (observation) parameters
        self.register_buffer('_obs_cov_chol', None)
        self.register_buffer('_obs_cov_inv', None)
        self.obs_parameters = obs_parameters

        # Register result parameters
        self.jump_times = None
        self.mjp_path = None
        self.mjp_path_discretized = None
        self.time_grid = None
        self.sde_path = None
        self.belief_filter = None
        self.belief_smoother = None
        self.samples = dict(mjp_jump_times=[],
                            mjp_paths=[],
                            sde_time=[],
                            sde_paths=[],
                            diffusion_slope=[],
                            diffusion_intercept=[],
                            diffusion_cov=[],
                            diffusion_init_mean=[],
                            diffusion_init_cov=[],
                            mjp_init_dist=[],
                            rate_mtx=[],
                            obs_cov=[])

        # Store input values
        self.rate_mtx = torch.as_tensor(rate_mtx, dtype=torch.float64)
        self.initial_posterior_mjp = torch.as_tensor(initial_posterior_mjp, dtype=torch.float64)

        self.diffusion_slope = torch.as_tensor(diffusion_slope, dtype=torch.float64)
        self.diffusion_intercept = torch.as_tensor(diffusion_intercept, dtype=torch.float64)
        self.diffusion_cov = torch.as_tensor(diffusion_cov, dtype=torch.float64)
        self.diffusion_cov_q = torch.tensor(diffusion_cov,
                                            dtype=torch.float64)  # Note: tensor() instead of as_tensor() as otherwise, the same entities are accessed by _q and non_q objects

        self.initial_posterior_mean = torch.as_tensor(initial_posterior_mean, dtype=torch.float64)
        self.initial_posterior_cov = torch.as_tensor(initial_posterior_cov, dtype=torch.float64)

        self.obs_cov = torch.as_tensor(obs_cov, dtype=torch.float64)
        self.obs_cov_q = torch.tensor(obs_cov, dtype=torch.float64)
        self.t_span = t_span
        self.t_obs = torch.as_tensor(t_obs, dtype=torch.float64)
        self.obs_data = torch.as_tensor(obs_data, dtype=torch.float64)

        if Optimizer_constructor is None:
            Optimizer_constructor = Adam
            if optimizer_opts is None:
                optimizer_opts = dict(lr=1e-4)

        if optimizer_opts is None:
            optimizer_opts = dict()

        self.optimizer = Optimizer_constructor(self.parameters(), **optimizer_opts)

    def discrete_state(self, t):
        """
        Return the discrete Z-state at a given time point t.
        :return: np.ndarray of length 1
        """
        jump_count = len(self.jump_times)
        if jump_count == 0:
            if type(t) is float or t.ndim == 0:
                z = np.zeros(1, dtype='int')
                z[0] = self.mjp_path[0]
            else:
                z = np.repeat(self.mjp_path, len(t))
            return z
        else:
            if type(t) is float or t.ndim == 0:
                z = np.zeros(1, dtype='int')
                z[0] = self.mjp_path[int(np.searchsorted(self.jump_times, t))]
            else:
                z = np.array(self.mjp_path)[np.searchsorted(self.jump_times, t)]
            return z

    # --------------------------------------------------------------------------------
    # --------------- 0. Getters and Setters ---------------
    # --------------------------------------------------------------------------------

    @property
    def rate_mtx(self):
        Q = self._rate_mtx
        Q = Q - torch.diag(torch.diag(Q))
        Q = Q - torch.diag(Q.sum(1))
        return Q

    @property
    def log_rate_mtx(self):
        Q = self._rate_mtx
        Q = Q - torch.diag(torch.diag(Q))
        Q = Q + torch.diag(torch.ones(self.n_states))
        return torch.log(Q)

    @rate_mtx.setter
    def rate_mtx(self, val):
        val = torch.as_tensor(val, dtype=torch.float64)
        assert val.ndim == 2, 'Required input format for Q matrix is [n_states, n_states]'
        assert val.shape[0] == val.shape[1], 'Q matrix must be symmetric'
        assert torch.isclose(val.sum(1),
                             torch.zeros(val.shape[0], dtype=torch.float64)).all(), 'Q matrix has fulfil row-sum-zero'
        self.n_states = val.shape[0]
        if self._rate_mtx is None:
            self._rate_mtx = val
        else:
            self._rate_mtx.data = val
        self.rate_mtx_zerodiag = val - torch.diag(torch.diag(val))

    @property
    def diffusion_cov_chol(self):
        return self._diffusion_cov_chol

    @property
    def diffusion_cov_inv(self):
        return torch.inverse(self._diffusion_cov_chol @ self._diffusion_cov_chol.transpose(-1, -2))

    @property
    def diffusion_cov(self):
        L = torch.tril(self.diffusion_cov_chol)
        L_T = L.transpose(-1, -2)
        return L @ L_T

    @diffusion_cov.setter
    def diffusion_cov(self, val):
        assert val.ndim == 3, 'Required input format for diffusion_cov is [n_states, n_dim, n_dim]'
        self.n_dim = val.shape[1]
        L = torch.cholesky(val)
        self.diffusion_cov_chol = L

    @diffusion_cov_chol.setter
    def diffusion_cov_chol(self, val):
        if self._diffusion_cov_chol is None:
            self._diffusion_cov_chol = nn.Parameter(val)
        else:
            self._diffusion_cov_chol.data = val

    @property
    def diffusion_cov_chol_q(self):
        return self._diffusion_cov_chol_q

    @property
    def diffusion_cov_inv_q(self):
        return self._diffusion_cov_inv_q

    @property
    def diffusion_cov_q(self):
        L = torch.tril(self._diffusion_cov_chol_q)
        L_T = L.transpose(-1, -2)
        return L @ L_T

    @diffusion_cov_q.setter
    def diffusion_cov_q(self, val):
        assert val.ndim == 3, 'Required input format for diffusion_cov is [n_states, n_dim, n_dim]'
        self.n_dim = val.shape[1]
        L = torch.cholesky(val)
        if self._diffusion_cov_chol_q is None:
            self._diffusion_cov_chol_q = L
        else:
            self._diffusion_cov_chol_q.data = L
        self._diffusion_cov_inv_q = torch.inverse(L @ L.transpose(-1, -2))

    @property
    def obs_cov(self):
        L = torch.tril(self._obs_cov_chol)
        L_T = L.transpose(-1, -2)
        return L @ L_T

    @property
    def obs_cov_chol(self):
        return self._obs_cov_chol

    @obs_cov_chol.setter
    def obs_cov_chol(self, val):
        if self._obs_cov_chol is None:
            self._obs_cov_chol = val
        else:
            self._obs_cov_chol.data = val
        self._obs_cov_inv = torch.inverse(self._obs_cov_chol @ self._obs_cov_chol.transpose(-1, -2))

    @obs_cov.setter
    def obs_cov(self, val):
        assert val.ndim == 2, 'Required input format for obs_cov is [n_dim, n_dim]'
        L = torch.cholesky(val)
        self.obs_cov_chol = L

    @property
    def obs_cov_inv(self):
        return self._obs_cov_inv

    @property
    def diffusion_slope(self):
        return self._diffusion_slope

    @diffusion_slope.setter
    def diffusion_slope(self, val):
        self._diffusion_slope = val

    @property
    def diffusion_intercept(self):
        return self._diffusion_intercept

    @diffusion_intercept.setter
    def diffusion_intercept(self, val):
        self._diffusion_intercept = val

    # 6) ------------ Posterior mjp model properties ------------
    @property
    def initial_posterior_mjp(self):
        return self.log_initial_posterior_mjp.exp()

    @property
    def log_initial_posterior_mjp(self):
        return torch.cat([self._initial_posterior_mjp_params,
                          torch.log1p(-torch.clamp(self._initial_posterior_mjp_params.exp().sum(0, keepdim=True), EPS,
                                                   1. - EPS))])

    @initial_posterior_mjp.setter
    def initial_posterior_mjp(self, val):
        self._initial_posterior_mjp_params = val.log()[:-1]

    @property
    def t_obs(self):
        return self._t_obs

    @t_obs.setter
    def t_obs(self, val):
        assert len(
            set(val).intersection(set(self.t_span))) == 0, 'Observation(s) on boundaries. Catch this special case.'
        val = torch.as_tensor(val, dtype=torch.float64)
        self._t_obs = val
        self._n_intervals = len(val) + 1
        self._interval_boundaries = [self.t_span[0]] + val.tolist() + [self.t_span[1]]

    @property
    def n_intervals(self):
        return self._n_intervals

    @property
    def interval_boundaries(self):
        return self._interval_boundaries

    @staticmethod
    def discretize_mjp_trajectories(jump_times, state_trajectories, t_span, dt):
        n_trajectories = len(state_trajectories)

        n_timesteps = int((t_span[1] - t_span[0]) / dt) + 1
        timesteps = np.linspace(t_span[0], t_span[1], n_timesteps)
        state_paths_discretized = np.zeros((n_trajectories, n_timesteps), dtype=int)
        for n in range(n_trajectories):
            insertion_indices = np.searchsorted(timesteps, jump_times[n])
            indices = np.concatenate((np.array([0]), insertion_indices, np.array([len(timesteps) - 1])))
            for pair_idx, pair in enumerate(zip(indices[:-1], indices[1:])):
                state_paths_discretized[n, pair[0]:pair[1]] = int(state_trajectories[n][pair_idx])

        return state_paths_discretized, timesteps

    # --------------------------------------------------------------------------------
    # ---------- 1. MJP Inference
    # --------------------------------------------------------------------------------

    # 1.1.1 Forward Wonham MJP Filtering (non-robust)
    # --------------------------------------------------------------------------------
    def filter_forward(self,
                       otraj: Union[np.ndarray, torch.Tensor],
                       time_grid: Union[np.ndarray, torch.Tensor],
                       initial_belief: Union[np.ndarray, torch.Tensor]):
        """
        Forward filtering as presented in Anderson

        :param opath: Observed trajectory
        :param timegrid: Timegrid of observed trajectory
        :param initial_belief: Initial value of filtering SPDE
        :return: None
        """
        self.time_grid = time_grid
        self.otraj = otraj

        dt = float(time_grid[1] - time_grid[0])
        eps = 1e-8  # TODO

        belief_traj = jit_wonham(initial_belief.numpy(), self.diffusion_slope.numpy(), self.diffusion_intercept.numpy(),
                                 self.rate_mtx.numpy(), self.diffusion_cov_inv_q.numpy(), otraj.numpy(), dt, eps)

        self.belief_filter = interpolate.interp1d(self.time_grid, belief_traj, axis=-1, bounds_error=False,
                                                  kind='previous', fill_value=(belief_traj[:, 0], belief_traj[:, -1]))

    # 1.1.2 Backward MJP smoothing
    # --------------------------------------------------------------------------------
    def smoother_backward(self,
                          otraj: torch.Tensor,
                          time_grid: torch.Tensor,
                          initial_belief: torch.Tensor
                          ):
        '''
        Backward smoother as presented in Anderson, eq. (6.7).

        :param otraj: Observation trajectory
        :param time_grid: Timegrid of observation trajectory
        :param initial_belief: End-point condition for backward ODE.
        :return: None
        '''

        t_span_rev = [time_grid[-1], time_grid[0]]
        self.filter_forward(otraj, time_grid, initial_belief)
        belief_filter_final = self.belief_filter(t_span_rev[0])

        self.posterior_rate_mtx = PosteriorRateMtx(self.rate_mtx_zerodiag.numpy(),
                                                   self.belief_filter,
                                                   np.asarray(time_grid))

        res = integrate.solve_ivp(self._smoother_ode, t_span_rev, belief_filter_final,
                                  method='BDF', dense_output=True)  # TODO: vectorized=True

        self.belief_smoother = res.sol

    def _smoother_ode(self, t: float, state: np.ndarray):
        p_s = state
        A = self.posterior_rate_mtx(t)

        rhs = -1 * A.T @ p_s

        return rhs

    # 1.1.3 Backward Posterior MJP Sampling using the forward filter computed above
    # --------------------------------------------------------------------------------
    # TODO doc
    def _sample_posterior_mjp(self, t_span, initial_belief) -> Tuple[List, List]:
        trajectory = []
        jump_times = []
        t_start, t_end = t_span
        t_query_rate = np.linspace(t_start, t_end, 1000)

        z_init = np.random.choice(np.arange(self.n_states), p=initial_belief / np.sum(initial_belief))
        trajectory.append(z_init)
        t_cur = t_start
        while t_cur < t_end:
            # 1. Set upper bound (overall maxmimum rate from the current time point onwards)
            t_cur_idx = np.where(t_query_rate >= t_cur)[0][0]
            future_posterior_rate_mtxs = self.posterior_rate_mtx(t_query_rate[t_cur_idx:])  # Dimension ZxZ'xT
            # Get diagonal view
            future_exit_rates = np.einsum('iij->ij', future_posterior_rate_mtxs)  # Dimension ZxT

            lambda_star = np.amax(np.abs(future_exit_rates))

            # 2. Sample inter-arrival time
            u = np.random.uniform()
            tau = -1 * np.log(u) / lambda_star

            # 3. Update current time
            t_cur += tau

            # Get instantaneous rate at new time
            posterior_rate_mtx = self.posterior_rate_mtx(t_cur)  # Dimension ZxZ'
            # Get diagonal view
            exit_rates = np.einsum('jj->j', posterior_rate_mtx)  # Dimension Z

            lambda_t = np.abs(exit_rates[trajectory[-1]])

            # Compare instantaneous to maximum rate and accept the jump time with probability lambda_t/lambda_star
            s = np.random.uniform()
            if s <= lambda_t / lambda_star:
                jump_times.append(t_cur)

                # find next state
                transition_probabilities = (np.eye(self.n_states) + posterior_rate_mtx / lambda_t)[trajectory[-1]]
                z_next = np.random.choice(np.arange(self.n_states), p=transition_probabilities)
                trajectory.append(z_next)

        return jump_times, trajectory

    # TODO doc
    def _sample_posterior_mjp_reverse(self, t_span, initial_belief) -> Tuple[List, List]:
        trajectory = []
        jump_times = []
        t_start, t_end = t_span
        assert t_start > t_end
        t_query_rate = np.linspace(t_start, t_end, 1000)

        z_init = np.random.choice(np.arange(self.n_states), p=initial_belief / np.sum(initial_belief))
        trajectory.append(z_init)
        t_cur = t_start
        while t_cur > t_end:
            # 1. Set upper bound (overall maxmimum rate from the current time point onwards)
            t_cur_idx = np.where(t_query_rate <= t_cur)[0][-1]  # TODO use searchsorted
            future_posterior_rate_mtxs = self.posterior_rate_mtx(t_query_rate[:t_cur_idx])  # Dimension ZxZ'xT
            # Get diagonal view
            future_exit_rates = np.einsum('iij->ij', future_posterior_rate_mtxs)  # Dimension ZxT

            lambda_star = np.amax(np.abs(future_exit_rates))

            # 2. Sample inter-arrival time
            u = np.random.uniform()
            tau = -1 * np.log(u) / lambda_star

            # 3. Update current time
            t_cur -= tau

            # Get instantaneous rate at new time
            posterior_rate_mtx = self.posterior_rate_mtx(t_cur)  # Dimension ZxZ'
            # Get diagonal view
            exit_rates = np.einsum('jj->j', posterior_rate_mtx)  # Dimension Z

            lambda_t = np.abs(exit_rates[trajectory[-1]])

            # Compare instantaneous to maximum rate and accept the jump time with probability lambda_t/lambda_star
            s = np.random.uniform()
            if s <= lambda_t / lambda_star:
                jump_times.append(t_cur)

                # find next state
                transition_probabilities = (np.eye(self.n_states) + posterior_rate_mtx / lambda_t)[trajectory[-1]]
                z_next = np.random.choice(np.arange(self.n_states), p=transition_probabilities)
                trajectory.append(z_next)

        return jump_times, trajectory

    # TODO doc
    def _sample_prior_mjp(self) -> Tuple[List, List]:
        trajectory = []
        jump_times = []
        t_start, t_end = self.t_span

        z_init = np.random.choice(np.arange(self.n_states), p=self.initial_posterior_mjp.numpy())
        trajectory.append(z_init)
        t_cur = t_start
        while t_cur < t_end:
            z_cur = trajectory[-1]
            Q_xy = self.rate_mtx_zerodiag[z_cur].numpy()
            Q_xx = -1 * np.sum(Q_xy)

            sojourn_time = np.random.exponential(
                -1 * 1 / Q_xx)  # Note: the numpy version uses shape parameterization
            t_next = t_cur + sojourn_time
            z_next = np.random.choice(np.arange(self.n_states), p=-1 * Q_xy / Q_xx)

            if t_next <= t_end:
                trajectory.append(z_next)
                jump_times.append(t_next)
            t_cur = t_next

        return jump_times, trajectory

    # 1.2.1 Backward MJP Filtering (van Handel)
    # --------------------------------------------------------------------------------
    def filter_backward(self,
                        otraj: torch.Tensor,
                        time_grid: torch.Tensor):
        self.otraj = otraj  # TODO is set twice, once in _sample_mjp
        self.time_grid = time_grid

        t_span_rev = [time_grid[-1], time_grid[0]]
        end_point_condition = np.exp(
            (self.diffusion_slope @ otraj[:, -1] + self.diffusion_intercept).squeeze() * otraj[:, -1].flatten())
        # end_point_condition = np.ones(self.n_states)

        res = integrate.solve_ivp(self._bwd_filter_ode, t_span_rev, np.asarray(end_point_condition),
                                  args=(np.asarray(otraj), np.asarray(time_grid)), method='BDF',
                                  dense_output=True)

        self.reverse_likelihood = res.sol

    def _bwd_filter_ode(self, t: float, state: np.ndarray, otraj: np.ndarray, time_grid: np.ndarray):

        t_idx = np.searchsorted(time_grid, t)
        y_t = otraj[:, t_idx]
        h = (self.diffusion_slope.detach().numpy() @ y_t + self.diffusion_intercept.detach().numpy())  # Dim Z x N
        h = h.squeeze()
        # TODO this works only for 1D Y-processes h(a_i) -> h(a_i, y_t)
        rhs = -1 * (self.rate_mtx * np.exp((h[:, None] - h[None, :]) * y_t)) @ state + 0.5 * h * h * state
        # rhs = -1 * (self.rate_mtx * np.exp((h[:, None] - h[None, :])*y_t)) @ state + 0.5 * h * h * state #+ self.diffusion_slope.detach().numpy().flatten() * y_t * dy_t * state

        return rhs

    def _bwd_filter_ode_log(self, t: float, state: np.ndarray, otraj: np.ndarray, time_grid: np.ndarray):

        t_idx = np.searchsorted(time_grid, t)
        y_t = otraj[:, t_idx]

        v = np.exp(state) * np.asarray(np.exp(-1 * self.diffusion_slope @ y_t))

        rhs = -1 * (self.rate_mtx @ v) / v + 0.5 * (self.diffusion_slope ** 2).flatten()

        return rhs

    def _bwd_filter_ode_v(self, t: float, state: np.ndarray, otraj: np.ndarray, time_grid: np.ndarray):

        t_idx = np.searchsorted(time_grid, t)
        y_t = otraj[:, t_idx]
        dy = np.diff(otraj, axis=1) / (time_grid[1] - time_grid[0])
        dy_t = dy[:, t_idx]

        rhs = -1 * self.rate_mtx @ state + 0.5 * (self.diffusion_slope ** 2).sum(1) * state - state * (
                np.asarray(self.diffusion_slope) @ dy_t)  # TODO generalize
        return np.asarray(rhs)

    def reverse_likelihood_mtx(self, t: Union[np.ndarray, float]):
        t = np.asarray(t)

        indices = np.searchsorted(self.time_grid, t, side='right')
        v_tilde = self.reverse_likelihood(t)  # Dim Z x T
        y_t = self.otraj[:, indices]  # Dim N x T
        h = (self.diffusion_slope.detach().numpy() @ y_t + self.diffusion_intercept.detach().numpy()[:, None,
                                                           :])  # Dim Z x N x T
        h = h.squeeze(1)

        # res = np.exp((self.diffusion_slope[:, None, :] - self.diffusion_slope[None, :, :]) @ y_t + v_tilde[None, :, :] - v_tilde[:, None, :])
        # res = np.exp((self.diffusion_slope[:, None, :] - self.diffusion_slope[None, :, :]) @ y_t) * v_tilde[None, :, :]/v_tilde[:, None, :]
        # res = v_tilde[None, :, :] / v_tilde[:, None, :]
        res = np.exp((h[:, None, :] - h[None, :, :]) * y_t[None, ...]) * v_tilde[None, :, :] / v_tilde[:, None, :]

        return res

    # 1.1.2 Forward MJP Smoothing using the backward filter computed above (van Handel)
    # --------------------------------------------------------------------------------
    def smoother_forward(self,
                         otraj: torch.Tensor,
                         time_grid: torch.Tensor):
        self.filter_backward(otraj, time_grid)
        t_span = [time_grid[0], time_grid[-1]]

        self.posterior_rate_mtx = PosteriorForwardRateMtx(self.rate_mtx_zerodiag.numpy(),
                                                          self.reverse_likelihood_mtx,
                                                          np.asarray(time_grid))

        initial_condition = np.array([0.5, 0.5]) * self.reverse_likelihood(t_span[0])
        initial_condition /= np.sum(initial_condition)

        res = integrate.solve_ivp(self._forward_smoother_ode, t_span, initial_condition,  # TODO real initial cond
                                  method='BDF', dense_output=True)  # TODO: vectorized=True

        self.belief_smoother_forward = res.sol

    def _forward_smoother_ode(self, t: float, state: np.ndarray):

        A = self.posterior_rate_mtx(t)
        rhs = A.T @ state

        return rhs

    # --------------------------------------------------------------------------------
    # -------------------- 2. SDE Inference
    # --------------------------------------------------------------------------------

    # 2.1.1 Forward Kalman filtering
    # --------------------------------------------------------------------------------
    def kalman_filter(self,
                      initial_mean: Union[np.ndarray, torch.Tensor],
                      initial_cov: Union[np.ndarray, torch.Tensor]
                      ):
        # TODO integrate odes for mean and covariance piecewise forward (between obs)
        initial_condition = np.concatenate((np.array(initial_mean).flatten(), np.array(initial_cov).flatten()))

        # Solve joint ODE backward in time
        joint_solution_list = []
        for interval in (range(self.n_intervals)):
            t_span = self.interval_boundaries[interval], self.interval_boundaries[interval + 1]
            joint_solution = integrate.solve_ivp(self._ode_kalman, t_span, initial_condition,
                                                 method='BDF', dense_output=True, vectorized=True)

            joint_solution_list.append(joint_solution.sol)

            # Reset Conditions
            if interval < len(self.t_obs):
                # Set variables

                # observation
                t_i = self.t_obs[interval].numpy()
                x_i = self.obs_data[:, interval][:, None].numpy()  # Dimension NxN'

                mean, cov = np.split(joint_solution.sol(t_i), [self.n_dim])
                mean = mean[:, None]  # Dim N x N'
                cov = cov.reshape((self.n_dim, self.n_dim))  # Dim N x N'

                # Note: nomenclature as in Särkkä, p. 216, eq. (10.69)
                v = x_i - mean
                S = cov + self.obs_cov.detach().numpy()
                K = cov @ np.linalg.inv(S)
                mean_updated = mean + K @ v
                cov_updated = cov - K @ S @ K.T
                initial_condition = np.concatenate((mean_updated.flatten(),
                                                    cov_updated.flatten()))

        joint_solution = cat_ode_solutions(joint_solution_list, shape=(
                self.n_dim * self.n_dim + self.n_dim + 1))

        self.kalman_mean = ReshapedOdeSolution(joint_solution, shape=(self.n_dim),
                                               sub_idxs=[0, self.n_dim])
        self.kalman_cov = ReshapedOdeSolution(joint_solution, shape=(self.n_dim, self.n_dim),
                                              sub_idxs=[self.n_dim, self.n_dim + int(self.n_dim ** 2)])

    def _ode_kalman(self, t: float, x: np.ndarray):
        mean, cov = np.split(x, [self.n_dim])

        if x.ndim == 2:
            is_batch = True
            num_batches = x.shape[1]
        else:
            is_batch = False
            num_batches = 1

        # Reshape to original space
        mean = mean.T.reshape((num_batches, self.n_dim))[:, None, :]  # Dimension BxN'xN
        cov = cov.T.reshape((num_batches, self.n_dim, self.n_dim))  # Dim BxNxN'

        z = int(self.discrete_state(t))
        F = self.diffusion_slope[z].numpy()[None, ...]  # Dim B x N x N'
        F_T = np.moveaxis(F, -2, -1)  # Dim B x N' x N
        b = self.diffusion_intercept[z].numpy()[None, :]  # Dim B x N

        rhs_mean = (F @ mean + b).reshape((num_batches, -1))
        rhs_cov = (F @ cov + cov @ F_T + self.diffusion_cov_q.numpy()).reshape((num_batches, -1))

        # Concatenate Solution
        res = np.concatenate((rhs_mean, rhs_cov), axis=1)  # Dimension B x (N + N^2)

        if is_batch:
            res = res.T  # Dimension  (N + N^2) x B
        else:
            res = res.squeeze()  # Dimension  N + N^2

        return res

    # 2.1.2 Backward RTS smoothing
    # --------------------------------------------------------------------------------
    def RTS_smoother(self):
        final_mean = self.kalman_mean(self.t_span[1])
        final_cov = self.kalman_cov(self.t_span[1])
        final_condition = np.concatenate((final_mean.flatten(), final_cov.flatten()))

        # Solve RTS ODE backward in time
        joint_solution_list = []
        for interval in reversed(range(self.n_intervals)):
            t_span = self.interval_boundaries[interval + 1], self.interval_boundaries[interval]
            joint_solution = integrate.solve_ivp(self._ode_rts_backward, t_span, final_condition,
                                                 method='BDF', dense_output=True, vectorized=True)

            joint_solution_list.append(joint_solution.sol)
            final_condition = joint_solution.sol(t_span[1])

        joint_solution = cat_ode_solutions(joint_solution_list, shape=(self.n_dim * self.n_dim + self.n_dim))

        self.rts_mean = ReshapedOdeSolution(joint_solution, shape=(self.n_dim),
                                            sub_idxs=[0, self.n_dim])
        self.rts_cov = ReshapedOdeSolution(joint_solution, shape=(self.n_dim, self.n_dim),
                                           sub_idxs=[self.n_dim, self.n_dim + int(self.n_dim ** 2)])

    def _ode_rts_backward(self, t: np.float, x: np.ndarray):
        mean, cov = np.split(x, [self.n_dim])

        if x.ndim == 2:
            is_batch = True
            num_batches = x.shape[1]
        else:
            is_batch = False
            num_batches = 1

        # Reshape to original space
        mean = mean.T.reshape((num_batches, self.n_dim))[:, None, :]  # Dimension BxN'xN
        cov = cov.T.reshape((num_batches, self.n_dim, self.n_dim))  # Dim BxNxN'

        z = int(self.discrete_state(t))
        F = self.diffusion_slope[z].numpy()  # Dim B x N x N'
        b = self.diffusion_intercept[z].numpy()[None, :]  # Dim B x N

        mean_kalman = self.kalman_mean(t)[None, :, None]  # Dim B x N x N'
        cov_kalman_inv = np.linalg.inv(self.kalman_cov(t))[None, ...]  # Dim B x N x N'
        F_eff = F + self.obs_cov.detach().numpy() @ cov_kalman_inv  # Dim
        F_eff_T = np.moveaxis(F_eff, -2, -1)

        rhs_mean = (F @ mean + b + self.obs_cov.detach().numpy() @ cov_kalman_inv @ (mean - mean_kalman)).reshape(
            (num_batches, -1))
        rhs_cov = (F_eff @ cov + cov @ F_eff_T - self.obs_cov.detach().numpy()).reshape((num_batches, -1))

        # Concatenate Solution
        res = np.concatenate((rhs_mean, rhs_cov), axis=1)  # Dimension B x (N + N^2)

        if is_batch:
            res = res.T  # Dimension  (N + N^2) x B
        else:
            res = res.squeeze()  # Dimension  N + N^2

        return res

    # 2.2.1 Backward information filter by van der Meulen (Thm 2.5)
    # ------------------------------------------------------------
    def bwd_information_filter(self):
        # and finally, log p_tilde(t, y) = -c(t) - 0.5 y.T H(t) y + F(t).T y

        # Set final states
        EPS = 1e2
        final_information = 1 / EPS * np.eye(
            self.n_dim).flatten()  # TODO this might fail for some numerical bullshit reason
        final_F = np.zeros(self.n_dim)
        final_c = np.ones(1) * (2 * np.pi) ** (-1 * self.n_dim) * np.linalg.det(EPS * np.eye(self.n_dim)) ** (-1 / 2)
        numel_info = self.n_dim ** 2
        numel_F = self.n_dim
        numel_c = 1

        # Concatenate final states for joint ODE system
        final_joint_state = np.concatenate((final_information,
                                            final_F,
                                            final_c))

        # Solve joint ODE backward in time
        joint_solution_list = []
        for interval in reversed(range(self.n_intervals)):
            t_span = self.interval_boundaries[interval + 1], self.interval_boundaries[interval]
            joint_solution = integrate.solve_ivp(self._ode_backward, t_span, final_joint_state,
                                                 method='BDF', dense_output=True, vectorized=True)

            joint_solution_list.append(joint_solution.sol)

            # Reset Conditions TODO
            if interval > 0:
                # Set variables

                # observation
                t_i = self.t_obs[interval - 1].numpy()
                x_i = self.obs_data[:, interval - 1][:, None].numpy()  # Dimension N'xN

                information, F, c = np.split(joint_solution.sol(t_i), [numel_info, numel_info + numel_F])
                final_information = information + self.obs_cov_inv.numpy().flatten()
                final_F = F + (self.obs_cov_inv.numpy() @ x_i).flatten()
                final_c = c + (0.5 * self.n_dim * np.log(2 * np.pi) + 0.5 * np.log(np.linalg.det(
                    self.obs_cov.numpy())) + 0.5 * x_i.T @ self.obs_cov_inv.numpy() @ x_i).flatten()

                final_joint_state = np.concatenate((final_information,
                                                    final_F,
                                                    final_c))

        joint_solution = cat_ode_solutions(joint_solution_list, shape=(
                self.n_dim * self.n_dim + self.n_dim + 1))

        # updates variational mean and covariance by reshaping solutions from flattened to original space
        self.backward_information = ReshapedOdeSolution(joint_solution, shape=(self.n_dim, self.n_dim),
                                                        sub_idxs=[0, numel_info])
        self.backward_F = ReshapedOdeSolution(joint_solution, shape=(self.n_dim),
                                              sub_idxs=[numel_info, numel_info + numel_F])
        self.backward_c = ReshapedOdeSolution(joint_solution, shape=(1,),
                                              sub_idxs=[numel_info + numel_F, numel_info + numel_F + numel_c])

    def _ode_backward(self, t: float, joint_state: np.ndarray):
        '''
        Implements
            dH(t) = (-A(t).T H(t) - H(t)A(t) + H(t)L(t)L(t).T H(t))dt,
            dF(t) = (-A(t).T F(t) + H(t)L(t)L(t).T F(t) + H(t)b(t))dt,
            dc(t) = (b(t).T F(t) + 0.5 F(t).T L(t)L(t).T F(t) - 0.5 tr(H(t)L(t)L(t).T))dt,
        where L(t) is the dispersion of the "variational" process.
        '''
        # Split quantities
        numel_info = self.n_dim ** 2
        numel_F = self.n_dim
        information, F, c = np.split(joint_state, [numel_info, numel_info + numel_F])

        if joint_state.ndim == 2:
            is_batch = True
            num_batches = joint_state.shape[1]
        else:
            is_batch = False
            num_batches = 1

        z = self.discrete_state(t)

        # Reshape variables to the original space
        information = information.T.reshape((num_batches, self.n_dim, self.n_dim))  # Dimension BxN'xN
        F = F.T.reshape((num_batches, self.n_dim))[..., None]  # Dimension BxNxN'
        F_T = np.moveaxis(F, -2, -1)
        c = c.T.reshape((num_batches, 1))  # Dimension Bx1

        # Set variables
        A = self.diffusion_slope.numpy()[z][None, ...]  # Dim B x N x N
        A_T = np.moveaxis(A, -2, -1)  # Dimension BxN'xN
        Sigma = self.diffusion_cov_q[z].numpy()[None, ...]  # Dim B x N x N  # TODO Sigma not the same for all Z?
        b = self.diffusion_intercept.numpy()[z][None, ..., None]  # Dim B x N x N'
        b_T = np.moveaxis(b, -2, -1)  # Dimension BxN'xN

        rhs_information = (- A_T @ information - information @ A + information @ Sigma @ information).reshape(
            (num_batches, -1))
        rhs_F = (-A_T @ F + information @ Sigma @ F + information @ b).reshape((num_batches, -1))
        rhs_c = (b_T @ F + 0.5 * F_T @ Sigma @ F - 0.5 * (Sigma * information).sum(axis=(-2, -1),
                                                                                   keepdims=True)).reshape(
            (num_batches, -1))

        # Concatenate Solution
        res = np.concatenate((rhs_information, rhs_F, rhs_c), axis=1)  # Dimension B x Z*N+Z*N*N'+Z+Z'*Z''+Z'''

        if is_batch:
            res = res.T  # Dimension  Z*N+Z*N*N'+Z+Z'*Z''+Z''' x B
        else:
            res = res.squeeze()  # Dimension  Z*N+Z*N*N'+Z+Z'*Z''+Z'''

        return res

    # 2.2.1.A Backward covariance filter by van der Meulen (Thm 2.6)
    # ------------------------------------------------------------
    def bwd_covariance_filter(self):

        # Set final states
        EPS = 1e2
        final_mean = np.zeros(self.n_dim)
        final_cov = EPS * np.eye(self.n_dim).flatten()  # TODO this might fail for some numerical bullshit reason
        numel_mean = self.n_dim
        numel_cov = self.n_dim ** 2

        # Concatenate final states for joint ODE system
        final_joint_state = np.concatenate((final_mean,
                                            final_cov))

        # Solve joint ODE backward in time
        joint_solution_list = []
        for interval in reversed(range(self.n_intervals)):
            t_span = self.interval_boundaries[interval + 1], self.interval_boundaries[interval]
            joint_solution = integrate.solve_ivp(self._ode_backward_covariance_filter, t_span, final_joint_state,
                                                 method='BDF', dense_output=True, vectorized=True)

            joint_solution_list.append(joint_solution.sol)

            # Reset Conditions TODO
            if interval > 0:
                # Set variables

                # observation
                t_i = self.t_obs[interval - 1].numpy()
                x_i = self.obs_data[:, interval - 1][:, None].numpy()  # Dimension N'xN

                mean, cov = np.split(joint_solution.sol(t_i), [numel_mean])
                cov = cov.reshape((self.n_dim, self.n_dim))
                final_cov = cov - cov @ np.linalg.inv(self.obs_cov.detach().numpy() + cov) @ cov
                final_cov = final_cov.flatten()
                final_mean = final_cov @ (self.obs_cov_inv.detach().numpy() @ x_i + np.linalg.inv(cov) @ mean)

                final_joint_state = np.concatenate((final_mean,
                                                    final_cov))

        joint_solution = cat_ode_solutions(joint_solution_list, shape=(
                self.n_dim * self.n_dim + self.n_dim + 1))

        # updates variational mean and covariance by reshaping solutions from flattened to original space
        self.backward_mean = ReshapedOdeSolution(joint_solution, shape=(self.n_dim, self.n_dim),
                                                 sub_idxs=[0, numel_mean])
        self.backward_cov = ReshapedOdeSolution(joint_solution, shape=(self.n_dim),
                                                sub_idxs=[numel_mean, numel_mean + numel_cov])

    def _ode_backward_covariance_filter(self, t: float, joint_state: np.ndarray):
        '''
        Implements
            dP(t) = (A(t) P(t) + P(t) A(t).T - L(t)L(t).T dt
            dnu(t) = (A(t) nu(t) + b(t)) dt
        where L(t) is the dispersion of the "variational" process.
        '''
        # Split quantities
        numel_mean = self.n_dim
        numel_cov = self.n_dim ** 2
        mean, cov = np.split(joint_state, [numel_mean])

        if joint_state.ndim == 2:
            is_batch = True
            num_batches = joint_state.shape[1]
        else:
            is_batch = False
            num_batches = 1

        z = self.discrete_state(t)

        # Reshape variables to the original space
        cov = cov.T.reshape((num_batches, self.n_dim, self.n_dim))  # Dimension BxN'xN
        mean = mean.T.reshape((num_batches, self.n_dim))[..., None]  # Dimension BxNxN'
        mean_T = np.moveaxis(mean, -2, -1)

        # Set variables
        A = self.diffusion_slope.detach().numpy()[z][None, ...]  # Dim B x N x N
        A_T = np.moveaxis(A, -2, -1)  # Dimension BxN'xN
        Sigma = self.diffusion_cov_q.numpy()[None, ...]  # Dim B x N x N  # TODO Sigma not the same for all Z?
        b = self.diffusion_intercept.numpy()[z][None, ..., None]  # Dim B x N x N'
        b_T = np.moveaxis(b, -2, -1)  # Dimension BxN'xN

        rhs_mean = (A_T @ mean + b).reshape((num_batches, -1))
        rhs_cov = (A @ cov + cov @ A_T - Sigma).reshape((num_batches, -1))

        # Concatenate Solution
        res = np.concatenate((rhs_mean, rhs_cov), axis=1)  # Dimension B x N+N*N'

        if is_batch:
            res = res.T  # Dimension  Z*N+Z*N*N'+Z+Z'*Z''+Z''' x B
        else:
            res = res.squeeze()  # Dimension  Z*N+Z*N*N'+Z+Z'*Z''+Z'''

        return res

    # 2.2.2. Forward posterior SDE sampling using the backward information filter (van der Meulen)
    # --------------------------------------------------------------------------------
    def sample_posterior_diffusion(self, dt: float):
        N_timesteps = int((self.t_span[1] - self.t_span[0]) / dt) + 1
        time = np.linspace(self.t_span[0], self.t_span[1], N_timesteps)

        z = self.discrete_state(time)
        Sigma = self.diffusion_cov_q.numpy()
        H = self.backward_information(time)  # Dim N x N x T
        H = np.transpose(H, axes=(2, 0, 1))  # Dim T x N x N
        F = self.backward_F(time)  # Dim N x T
        F = np.moveaxis(F, 0, 1)  # Dim T x N

        slope = (self.diffusion_slope[z] - Sigma[z] @ H).numpy()
        intercept = (self.diffusion_intercept[z] + (Sigma[z] @ F[:, :, None]).squeeze(-1)).numpy()
        Q = self.diffusion_cov_chol_q[z].numpy()

        if self.n_dim == 1:
            dW = np.random.normal(scale=np.sqrt(dt), size=(N_timesteps))
            dW = dW[:, None]
        else:
            dW = np.random.multivariate_normal(np.zeros(self.n_dim), dt * np.eye(self.n_dim), size=(N_timesteps))

        # Draw initial point from posterior Gaussian, informed by backward solution beta(y, 0)
        Sigma_bar = np.linalg.inv(np.linalg.inv(self.initial_posterior_cov) + self.backward_information(0.))
        mu_bar = Sigma_bar @ (np.linalg.inv(self.initial_posterior_cov.numpy()) @ self.initial_posterior_mean.numpy() + self.backward_F(0.))
        y_0 = np.random.multivariate_normal(mean=mu_bar, cov=Sigma_bar)

        res = jit_euler_maruyama(y_0, slope, intercept, Q, dW, N_timesteps, dt)

        return time, res

    def _euler_maruyama(self, drift: Callable, dispersion: Callable,
                        initial_mean: Union[np.ndarray, torch.Tensor], initial_cov: Union[np.ndarray, torch.Tensor],
                        dt: float):
        N_timesteps = int((self.t_span[1] - self.t_span[0]) / dt)
        time = np.arange(self.t_span[0], self.t_span[1], dt)
        continuous_trajectory = np.zeros((self.n_dim, N_timesteps))

        y_0 = np.random.multivariate_normal(initial_mean.detach().numpy(), initial_cov.detach().numpy())

        continuous_trajectory[:, 0] = y_0

        if self.n_dim == 1:
            dW = np.random.normal(scale=np.sqrt(dt), size=(N_timesteps))
            dW = dW[:, None]
        else:
            dW = np.random.multivariate_normal(np.zeros(self.n_dim), dt * np.eye(self.n_dim), size=(N_timesteps))

        for i in range(1, N_timesteps):
            t_cur = time[i - 1]
            y_cur = continuous_trajectory[:, i - 1]
            y_next = (y_cur + drift(t_cur, y_cur) * dt + dispersion(t_cur) @ dW[i])
            continuous_trajectory[:, i] = y_next

        return continuous_trajectory

    def _prior_drift(self, t: float, x: np.ndarray):
        return self.diffusion_slope @ x + self.diffusion_intercept

    def _posterior_drift(self, t: float, x: Union[torch.Tensor, np.ndarray]):
        """
        Return the posterior drift a(t, X).

        :param t: time
        :param x: state
        :return: np.ndarray of same shape as x
        """
        # TODO make cov mode-dependent as in JIT versions
        z = self.discrete_state(t)
        Sigma = self.diffusion_cov_q.numpy()
        H = self.backward_information(t)
        F = self.backward_F(t)

        posterior_drift = (self.diffusion_slope[z] - Sigma @ H) @ x + self.diffusion_intercept[
            z] + Sigma @ F

        return posterior_drift.numpy()

    def _posterior_dispersion(self, t: float):
        return self.diffusion_cov_chol_q.numpy()

    # --------------------------------------------------------------------------------
    # -------------------- 3. Gibbs Sampling
    # --------------------------------------------------------------------------------

    def sample_posterior(self, dt: float):
        """
        Return samples from the posterior (conditional) path measures.

        :param dt: time step used for Euler-Maruyama
        :return: tuple, (<MJP jump times>, <MJP samples>, <SDE time grid>, <SDE samples>)
        """

        # 1. Sample Y|Z
        sde_time_grid, sde_traj = self.sample_posterior_conditional_diffusion(dt)

        # 2. Sample Z|Y
        mjp_jump_times, mjp_traj = self.sample_posterior_conditional_MJP(sde_time_grid, sde_traj)
        assert (np.diff(mjp_jump_times) >= 0).all()
        self.jump_times = mjp_jump_times
        self.mjp_path = mjp_traj

        # 3. Store both samples
        self.samples['mjp_jump_times'].append(mjp_jump_times)
        self.samples['mjp_paths'].append(mjp_traj)
        self.samples['sde_paths'].append(sde_traj)
        if len(self.samples['sde_time']) == 0:
            self.samples['sde_time'] = sde_time_grid

    def sample_posterior_conditional_MJP(self, time, traj):
        # 1. Forward filter for Z|Y
        self.filter_forward(torch.as_tensor(traj), time, self.initial_posterior_mjp)
        self.posterior_rate_mtx = PosteriorRateMtx(self.rate_mtx_zerodiag.numpy(),
                                                   self.belief_filter,
                                                   np.asarray(time), clip_above=100)
        # 2. Backward sample Z|Y
        jump_times, trajectory = jit_sample_posterior_mjp_reverse(self.t_span, self.belief_filter(self.t_span[1]),
                                                                  self.posterior_rate_mtx._interpolants,
                                                                  self.posterior_rate_mtx.time_grid)
        return jump_times, trajectory

    def sample_posterior_conditional_diffusion(self, dt):
        # 1. Backward filter for Y|Z
        self.bwd_information_filter()

        # Forward sample Y|Z
        time, traj = self.sample_posterior_diffusion(dt=dt)
        return time, traj

    # --------------------------------------------------------------------------------
    # -------------------- 4. Parameter sampling
    # --------------------------------------------------------------------------------
    def sample_parameters(self,
                          sample_initial_conditions=False,
                          sample_sde_drift_parameters=False,
                          update_sde_dispersion_parameters=False,
                          sample_mjp_parameters=False,
                          sample_observation_parameters=False):
        logging.debug('Sampling Parameters')
        if sample_initial_conditions:
            self.sample_diffusion_initial_conditions()
            self.sample_mjp_initial_conditions()
        if sample_sde_drift_parameters:
            self.sample_diffusion_parameters(update_sde_dispersion_parameters)
        if sample_mjp_parameters:
            self.sample_mjp_parameters()
        if sample_observation_parameters:
            self.sample_observation_parameters()

    def sample_diffusion_initial_conditions(self):
        m_0 = self.sde_init_parameters['N_location']
        k_0 = self.sde_init_parameters['N_scale']
        n_0 = self.sde_init_parameters['IW_dof']
        S_0 = self.sde_init_parameters['IW_scale_mtx']

        y_hat = self.samples['sde_paths'][-1][:, 0]

        k_post, n_post = k_0 + 1, n_0 + 1
        m_post = (k_0 * m_0 + y_hat) / (k_0 + 1)
        S_post = np.linalg.inv(np.linalg.inv(S_0) + (k_0 / (k_0 + 1) * (y_hat - m_0)[:, None] * (y_hat - m_0)[None, :]))

        cov_init = st.invwishart(df=n_post, scale=S_post).rvs()
        mu_init = st.multivariate_normal(mean=m_post, cov=cov_init / k_post).rvs()
        if self.n_dim == 1:
            cov_init = cov_init.reshape((1, 1))
            mu_init = mu_init.reshape((1,))

        self.samples['diffusion_init_mean'].append(mu_init)
        self.samples['diffusion_init_cov'].append(cov_init)
        self.initial_posterior_mean = torch.as_tensor(mu_init, dtype=torch.float64)
        self.initial_posterior_cov = torch.as_tensor(cov_init, dtype=torch.float64)

    def sample_mjp_initial_conditions(self):
        z_hat = torch.zeros(self.n_states, dtype=torch.float64)
        z_hat[self.samples['mjp_paths'][-1][0]] = 1.  # One-hot encoding

        mjp_dist_new = np.random.dirichlet(self.mjp_init_pseudocounts + z_hat.numpy())
        self.samples['mjp_init_dist'].append(mjp_dist_new)
        self.initial_posterior_mjp = torch.as_tensor(mjp_dist_new, dtype=torch.float64)

    def sample_diffusion_parameters(self, update_sde_dispersion_parameters: bool = False):

        dt = self.time_grid[1] - self.time_grid[0]
        z_hat_onehot = torch.zeros((self.n_states, self.time_grid.shape[0]), dtype=torch.float64)
        z_hat, _ = self.discretize_mjp_trajectories([self.samples['mjp_jump_times'][-1]],
                                                    [self.samples['mjp_paths'][-1]], self.t_span, dt)
        z_hat_onehot[z_hat, np.arange(self.time_grid.shape[0], dtype=int)] = 1
        z_hat_onehot = torch.as_tensor(z_hat_onehot, dtype=torch.float64)  # Dim Z x T
        y_hat = torch.as_tensor(self.samples['sde_paths'][-1], dtype=torch.float64)  # Dim N x T

        M_0 = self.sde_mn_parameters['MN_location']
        K_0 = self.sde_mn_parameters['MN_scale_mtx']

        M = np.zeros(M_0.shape)
        K = np.zeros(K_0.shape)

        m = MultivariateNormal(torch.zeros(self.n_dim), torch.eye(self.n_dim))
        slope_new = np.zeros((self.n_states, self.n_dim, self.n_dim))
        intercept_new = np.zeros((self.n_states, self.n_dim))
        cov_new = np.zeros((self.n_states, self.n_dim, self.n_dim))
        for z in range(self.n_states):
            Yz = y_hat[:, np.where(z_hat_onehot[z, :-1] == 1)[0] + 1].numpy()  # Dim N x Tz

            Yz_bar = np.ones((Yz.shape[0] + 1, Yz.shape[1]))  # Dim (N+1) x Tz
            Yz_bar[:-1, :] = y_hat[:, np.where(z_hat_onehot[z, :-1] == 1)[0]]

            Delta_Yz = Yz - Yz_bar[:self.n_dim, :]

            M[z] = (Delta_Yz @ Yz_bar.T + M_0[z] @ K_0[z]) @ np.linalg.inv(Yz_bar @ Yz_bar.T * dt + K_0[z])
            K[z] = Yz_bar @ Yz_bar.T * dt + K_0[z]

            if update_sde_dispersion_parameters:
                N = z_hat_onehot.sum(1)[z]

                Gamma = torch.zeros((self.n_dim, self.n_dim + 1), dtype=torch.float64)
                Gamma[:, :-1] = self.diffusion_slope[z]
                Gamma[:, -1] = self.diffusion_intercept[z]
                Delta_Yz = torch.as_tensor(Delta_Yz, dtype=torch.float64)
                Yz_bar = torch.as_tensor(Yz_bar, dtype=torch.float64)
                H = torch.as_tensor(self.backward_information(self.time_grid))  # Dim N x N x T
                F = torch.as_tensor(self.backward_F(self.time_grid))  # Dim N x T

                # 1. Define r = log(p(D|theta)p(theta)) = loss function in PyTorch
                r = -1 * (torch.logdet(2 * np.pi * self.diffusion_cov[z] * dt) * (-N / 2)
                          - 0.5 * torch.trace(
                            (Delta_Yz - Gamma @ Yz_bar * dt).T @ torch.inverse(self.diffusion_cov[z] * dt) @
                            (Delta_Yz - Gamma @ Yz_bar * dt))
                          - 0.5 * (self.sde_mn_parameters['IW_dof'] + self.n_dim + 1) * torch.logdet(
                            self.diffusion_cov[z])
                          - 0.5 * torch.trace(
                            self.sde_mn_parameters['IW_scale_mtx'][z] @ torch.inverse(self.diffusion_cov[z])))

                # 2. Compute grad_theta r
                self.optimizer.zero_grad()
                r.backward()
                grad_r = deepcopy(self.diffusion_cov_chol._grad[z])

                # 3. compute proposal theta_star; choose 0 < tau << 1 NOTE: random walk on CHOLESKY, not on covariance itself!
                tau = 1e-4
                theta_star = self.diffusion_cov_chol[z].detach() + tau * grad_r + np.sqrt(
                    2 * tau) * m.sample()
                theta_star.requires_grad = True
                cov_star = theta_star @ theta_star.T

                # 4. define r_star
                r_star = -1 * (torch.logdet(2 * np.pi * cov_star * dt) * (-N / 2)
                               - 0.5 * torch.trace(
                            (Delta_Yz - Gamma @ Yz_bar * dt).T @ torch.inverse(cov_star * dt) @
                            (Delta_Yz - Gamma @ Yz_bar * dt))
                               - 0.5 * (self.sde_mn_parameters['IW_dof'] + self.n_dim + 1) * torch.logdet(
                            cov_star)
                               - 0.5 * torch.trace(
                            self.sde_mn_parameters['IW_scale_mtx'][z] @ torch.inverse(cov_star)))

                # 5. compute grad_theta r_star
                self.optimizer.zero_grad()
                r_star.backward()
                grad_r_star = deepcopy(theta_star._grad)

                # 6. Compute q-ratio
                theta = deepcopy(self.diffusion_cov_chol[z].detach())
                diff1 = theta - theta_star - tau * grad_r_star
                diff2 = theta_star - theta - tau * grad_r
                with torch.no_grad():
                    log_q_ratio = -1/(4 * tau) * (torch.max(torch.as_tensor(np.linalg.svd((diff1.T @ diff1).detach(), compute_uv=False)))
                                                  - torch.max(torch.as_tensor(np.linalg.svd((diff2.T @ diff2).detach(), compute_uv=False))))

                # 7. Compute posterior ratio
                r_no_dt = (torch.logdet(2 * np.pi * self.diffusion_cov[z]) * (-N / 2)
                          - 0.5 * torch.trace(
                            (Delta_Yz - Gamma @ Yz_bar * dt).T @ torch.inverse(self.diffusion_cov[z] * dt) @
                            (Delta_Yz - Gamma @ Yz_bar * dt))
                          - 0.5 * (self.sde_mn_parameters['IW_dof'] + self.n_dim + 1) * torch.logdet(
                            self.diffusion_cov[z])
                          - 0.5 * torch.trace(
                            self.sde_mn_parameters['IW_scale_mtx'][z] @ torch.inverse(self.diffusion_cov[z])))

                r_star_no_dt = (torch.logdet(2 * np.pi * cov_star) * (-N / 2)
                               - 0.5 * torch.trace(
                            (Delta_Yz - Gamma @ Yz_bar * dt).T @ torch.inverse(cov_star * dt) @
                            (Delta_Yz - Gamma @ Yz_bar * dt))
                               - 0.5 * (self.sde_mn_parameters['IW_dof'] + self.n_dim + 1) * torch.logdet(
                            cov_star)
                               - 0.5 * torch.trace(
                            self.sde_mn_parameters['IW_scale_mtx'][z] @ torch.inverse(cov_star)))

                # 8. Evaluate acceptance rate alpha
                A = torch.exp(log_q_ratio + r_star_no_dt - r_no_dt)
                # 9. MH sampling
                if A > 1:
                    theta_new = theta_star.detach().numpy()
                    cov_new[z] = theta_new @ theta_new.T
                else:
                    u = np.random.uniform()
                    if u <= A:
                        theta_new = theta_star.detach().numpy()
                        cov_new[z] = theta_new @ theta_new.T
                    else:
                        theta_new = theta.detach().numpy()
                        cov_new[z] = theta_new @ theta_new.T

            # Draw posterior samples
            Gamma_new = np.random.multivariate_normal(mean=M[z].flatten(), cov=np.kron(np.linalg.inv(K[z]), cov_new[z]))
            Gamma_new = Gamma_new.reshape((self.n_dim, self.n_dim + 1))
            slope_new[z] = Gamma_new[:, :-1]
            intercept_new[z] = Gamma_new[:, -1]

        self.samples['diffusion_slope'].append(slope_new)
        self.samples['diffusion_intercept'].append(intercept_new)
        self.samples['diffusion_cov'].append(cov_new)
        self.diffusion_slope = torch.as_tensor(slope_new, dtype=torch.float64)
        self.diffusion_intercept = torch.as_tensor(intercept_new, dtype=torch.float64)
        self.diffusion_cov = torch.as_tensor(cov_new, dtype=torch.float64)

    def sample_mjp_parameters(self):
        z_hat = self.samples['mjp_paths'][-1]
        j_hat = [0] + self.samples['mjp_jump_times'][-1] + [self.t_span[1]]

        Lambda = np.zeros((self.n_states, self.n_states))
        N_zz = np.zeros((self.n_states, self.n_states))
        T_z = np.zeros(self.n_states)
        for z_from in range(self.n_states):
            from_idxs = np.where(np.array(z_hat) == z_from)[0]
            j_from = np.array(j_hat)[from_idxs]
            j_to = np.array(j_hat)[from_idxs + 1]

            T_z[z_from] = np.sum(j_to - j_from)
            for z_to in range(self.n_states):
                if z_to != z_from:
                    alpha = self.mjp_gamma_pseudocounts['shape']
                    beta = self.mjp_gamma_pseudocounts['rate']
                    for t in range(len(z_hat) - 1):
                        N_zz[z_from, z_to] += ((z_hat[t + 1] == z_to) & (z_hat[t] == z_from))

                    Lambda[z_from, z_to] = np.clip(np.random.gamma(shape=(alpha[z_from, z_to] + N_zz[z_from, z_to]),
                                                                   scale=1 / (beta[z_from, z_to] + T_z[z_from])),
                                                   a_min=EPS, a_max=None)

        diag = np.einsum('ii->i', Lambda)
        diag[:] = -1 * np.sum(Lambda, axis=1)

        self.samples['rate_mtx'].append(Lambda)
        self.rate_mtx = Lambda

    def sample_observation_parameters(self):
        # TODO write down math in
        x = self.obs_data  # Dim n_dim x T
        T = x.shape[1]
        t_obs = self.t_obs
        y_hat = torch.as_tensor(self.samples['sde_paths'][-1], dtype=torch.float64)  # Dim N x T

        idx = np.searchsorted(self.time_grid, t_obs, side='right')  # 'right' to make it Ito-conforming
        empirical_cov = (x - y_hat[:, idx]) @ (x - y_hat[:, idx]).T

        obs_cov_new = st.invwishart(df=(self.obs_parameters['IW_dof'] + T),
                                    scale=(self.obs_parameters['IW_scale_mtx'] + empirical_cov.numpy())).rvs()

        if self.n_dim == 1:
            obs_cov_new = obs_cov_new[None, None]

        self.samples['obs_cov'].append(obs_cov_new)
        self.obs_cov = torch.as_tensor(obs_cov_new, dtype=torch.float64)

    # --------------------------------------------------------------------------------
    # -------------------- 5. Fit function
    # --------------------------------------------------------------------------------
    def fit(self, N_samples: int, dt: float, sample_parameters: bool = False,
            opts_sample_params: Dict = None, checkpoint_interval: int = -1,
            checkpoint_name: str = '') -> None:
        """
        Run the sampler to produce N_samples.

        :param N_samples: int, number of samples
        :param dt: float, discretization time step for Euler-Maruyama
        :param sample_parameters: bool, whether to sample the model parameters
        :param opts_sample_params: dict, specifies which parameters to sample
        :param checkpoint_interval:
        :param checkpoint_name:
        :return: None
        """
        # Timestamp for checkpoints
        timestamp = datetime.now().strftime('%Y%m%d-%H%M%S')

        if opts_sample_params is None:
            opts_sample_params = dict(sample_initial_conditions=False,
                                      sample_sde_drift_parameters=False,
                                      sample_sde_dispersion_parameters=False,
                                      sample_mjp_parameters=False,
                                      sample_observation_parameters=False)

        logging.info('Starting sampler')

        # Generate first MJP path from prior
        if self.jump_times is None:
            jump_times, paths = self._sample_prior_mjp()
            self.jump_times = jump_times
            self.mjp_path = paths
            self.samples['mjp_jump_times'].append(jump_times)
            self.samples['mjp_paths'].append(paths)

        # Save initial parameters before starting the sampler
        self.samples['diffusion_init_mean'].append(self.initial_posterior_mean.numpy())
        self.samples['diffusion_init_cov'].append(self.initial_posterior_cov.numpy())
        self.samples['diffusion_slope'].append(self.diffusion_slope.numpy())
        self.samples['diffusion_intercept'].append(self.diffusion_intercept.numpy())
        self.samples['diffusion_cov'].append(self.diffusion_cov.detach().numpy())
        self.samples['mjp_init_dist'].append(self.initial_posterior_mjp.numpy())
        self.samples['rate_mtx'].append(self.rate_mtx.numpy())
        self.samples['obs_cov'].append(self.obs_cov.numpy())

        for iter in range(N_samples):
            # Sample posterior paths
            self.sample_posterior(dt=dt)

            # Update parameters
            if sample_parameters:
                self.sample_parameters(**opts_sample_params)

            # Save checkpoints
            if checkpoint_interval > 0 and (iter % checkpoint_interval == 0 or iter == N_samples - 1):
                torch.save(self,
                           '../' + CHECKPOINT_PATH + 'shs_fit_{}_{}_iter_{}.pt'.format(checkpoint_name, timestamp,
                                                                                       iter))
            logging.debug(f'>Sample {iter + 1} drawn')

        # Save final fit
        if checkpoint_interval > 0:
            torch.save(self,
                       '../' + CHECKPOINT_PATH + 'shs_fit_{}_{}_iter_final.pt'.format(checkpoint_name, timestamp))

        logging.info('Sampler finished')

    # --------------------------------------------------------------------------------
    # -------------------- 6. Solve the exact PDEs
    # --------------------------------------------------------------------------------
    def exact_posterior_density(self,
                                initial_density: np.ndarray,
                                space_lim: Union[List, Tuple, np.ndarray],
                                time_lim: Union[List, Tuple, np.ndarray],
                                ):
        """
        Return the exact posterior density computed via the Hybrid Master Equation and simple Finite Differences.

        :param initial_density: [n_space, n_modes] array specifying the discretized initial density for each mode.
                                    Summing over the 0-th axis must return 1 for each mode.
        :param space_lim: [2,] [lower limit, upper limit] of discretized space Y
        :return: [n_space, n_time]
        """
        n_states = initial_density.shape[1]
        n_modes = initial_density.shape[0]

        self.backward_hme(n_states, n_modes, space_lim)

        # As in VI: solve forward OE (TODO: copy code)
        self.forward_hme(initial_density, space_lim)

    def backward_hme(self, n_states: int, n_modes: int, space_lim: np.ndarray):
        initial_beta = np.ones(n_states * n_modes)
        state_grid = np.linspace(*space_lim, n_states)

        # Create piecewise ode solutions between data points
        state_list = []

        # Solve ODEs backward in time
        for interval in reversed(range(self.n_intervals)):
            t_span = self.interval_boundaries[interval + 1], self.interval_boundaries[interval]

            state = integrate.solve_ivp(self._ode_beta, t_span, initial_beta,
                                        args=(state_grid, n_modes), method='BDF', dense_output=True)

            state_list.append(state.sol)

            # Reset Conditions
            if interval > 0:
                # Observation
                t_i = self.t_obs[interval - 1]
                x_i = self.obs_data[:, interval - 1].numpy()  # Dimension Z x N' x N

                # Reset
                p_obs = st.norm(loc=x_i, scale=np.sqrt(self.obs_cov.flatten())).pdf(state_grid)
                initial_beta = state.sol(t_i).reshape((n_modes, n_states)) * p_obs[None, :]
                initial_beta = initial_beta.flatten()

        # Concatenate Piecewise Solutions
        self.backward_solution = cat_ode_solutions(state_list, shape=(n_modes, n_states))

    def _ode_beta(self, t: float, beta: np.ndarray, grid_points: np.ndarray, n_modes: int):
        n_states = grid_points.shape[0]
        beta = beta.reshape((n_modes, n_states))
        dy = grid_points[1] - grid_points[0]
        grid_points = grid_points[None, :]  # Dim Z x N
        rhs = np.ones((beta.shape))

        diffusion_slope = self.diffusion_slope.detach().numpy().squeeze(1)  # Dim Z x 1
        diffusion_intercept = self.diffusion_intercept.detach().numpy()  # Dim Z x 1
        diffusion_cov = self.diffusion_cov.detach().numpy().squeeze(1)  # Dim Z x 1
        rate_mtx = self.rate_mtx.detach().numpy()[:, :, None]  # Dim Z x Z' x 1

        f = diffusion_slope * grid_points + diffusion_intercept

        partial_y = (beta[:, 2:] - beta[:, :-2])/(2 * dy)
        partial2_y = (beta[:, 2:] - 2 * beta[:, 1:-1] + beta[:, :-2]) / (dy**2)

        rhs[:, 1:-1] = -1 * ((f[:, 1:-1] * partial_y) + 0.5 * diffusion_cov * partial2_y + (rate_mtx * beta[None, :, :]).sum(1)[:, 1:-1])
        rhs[:, 0] = rhs[:, 1]
        rhs[:, -1] = rhs[:, -2]

        return rhs.flatten()

    def forward_hme(self, initial_density: np.ndarray, space_lim: np.ndarray):
        n_modes, n_states = initial_density.shape
        state_grid = np.linspace(*space_lim, n_states)

        # Create piecewise ode solutions between data points
        state_list = []

        # Solve ODEs forward in time
        for interval in range(self.n_intervals):
            t_span = self.interval_boundaries[interval], self.interval_boundaries[interval + 1]

            state = integrate.solve_ivp(self._ode_hme, t_span, initial_density.flatten(),
                                        args=(state_grid, n_modes), method='BDF', dense_output=True)

            state_list.append(state.sol)
            initial_density = state.sol(t_span[1])

        # Concatenate Piecewise Solutions
        self.forward_solution = cat_ode_solutions(state_list, shape=(n_modes, n_states))

    def _ode_hme(self, t: np.float, density: np.ndarray, grid_points: np.ndarray, n_modes: int):
        n_states = grid_points.shape[0]
        density = density.reshape((n_modes, n_states))
        dy = grid_points[1] - grid_points[0]

        rhs = np.zeros(density.shape)

        diffusion_slope = self.diffusion_slope.detach().numpy().squeeze(1)  # Dim Z x 1
        diffusion_intercept = self.diffusion_intercept.detach().numpy()  # Dim Z x 1
        diffusion_cov = self.diffusion_cov.detach().numpy().squeeze(1)  # Dim Z x 1
        rate_mtx = self.rate_mtx.detach().numpy()
        np.fill_diagonal(rate_mtx, 0)
        rate_mtx = rate_mtx[:, :, None]  # Dim Z x Z' x 1
        beta = self.backward_solution(t)
        beta[beta < 0] = 1e-26 # EPS
        log_beta = np.log(beta)

        f = diffusion_slope * grid_points + diffusion_intercept # Dim Z x N
        f_tilde = f
        f_tilde[:, 1:-1] = f[:, 1:-1] + diffusion_cov * (log_beta[:, 2:] - log_beta[:, :-2]) / (2 * dy)

        rate_mtx_tilde = rate_mtx * beta[None, :, :] / beta[:, None, :]
        off_diag_sum = rate_mtx_tilde.sum(1)
        #off_diag_sum = np.moveaxis(off_diag_sum, 0, 1)
        diag = np.einsum('iit->it', rate_mtx_tilde)
        diag[:] = -1 * off_diag_sum

        partial_y = (f_tilde[:, 2:] * density[:, 2:] - f_tilde[:, :-2] * density[:, :-2]) / (2 * dy)
        partial2_y = (density[:, 2:] - 2 * density[:, 1:-1] + density[:, :-2]) / (dy**2)

        F = - partial_y + 0.5 * diffusion_cov * partial2_y
        T = (rate_mtx_tilde * density[:, None, :]).sum(0)[:, 1:-1]

        rhs[:, 1:-1] = F + T

        return rhs.flatten()

class PosteriorRateMtx:
    def __init__(self,
                 prior_rate_mtx_zerodiag: np.ndarray,
                 belief_filter: Callable,
                 time_grid: np.ndarray,
                 clip_above: float = None):
        """
        Class for the posterior rate matrix
        """
        self.clip_above = clip_above
        self.time_grid = time_grid
        self.belief_filter = belief_filter
        self.prior_rate_mtx_zerodiag = prior_rate_mtx_zerodiag

        self._interpolants = self.init_interpolants()

        _interpolants = self._interpolants.reshape((-1, self._interpolants.shape[-1]), order='F')
        self.interpolant_fn = interpolate.interp1d(self.time_grid, _interpolants, kind='previous', bounds_error=False,
                                                   fill_value=(_interpolants[:, 0], _interpolants[:, -1]))

    @property
    def interpolants(self):
        return self._interpolants

    def init_interpolants(self):
        # Get the belief interpolants
        p_f = self.belief_filter(self.time_grid)  # Dimension Z x T
        A = self.prior_rate_mtx_zerodiag  # Dimension ZxZ'
        # n_states = A.shape[0]
        # A += np.eye(n_states)

        # compute posterior mtx entries at these interpolant positions
        # interpolants = np.exp(np.log(p_f[None, :, :]) - np.log(p_f[:, None, :]) + np.log(A.T[:, :, None]))
        interpolants = (p_f[None, :, :] * A.T[:, :, None]) / p_f[:, None, :]

        if self.clip_above is not None:
            interpolants = np.clip(interpolants, a_min=None, a_max=self.clip_above)

        diag = np.einsum('iit->it', interpolants)
        diag[:] = -1 * np.sum(interpolants, axis=1)
        return interpolants

    def __call__(self, t: Any):
        """
         Evaluates the rate matrix according to #TODO

        :param t: time point(s) for evaluation can be float, singleton or array of size n_times
        :return: [n_states x n_states] or [n_states x n_states x n_time_points] posterior rate matrix
        """

        # Cast as numpy
        t = np.asarray(t)

        # interpolate the array
        res = self.interpolant_fn(t)

        if t.ndim == 0:
            res = res.reshape(self._interpolants.shape[:-1], order='F')
        else:
            res = res.reshape((*self._interpolants.shape[:-1], *t.shape), order='F')
        return res


class PosteriorForwardRateMtx:
    def __init__(self,
                 prior_rate_mtx_zerodiag: np.ndarray,
                 belief_filter: Callable,
                 time_grid: np.ndarray,
                 clip_above: float = None):
        """
        Class for the posterior rate matrix
        """
        self.clip_above = clip_above
        self.time_grid = time_grid
        self.belief_filter = belief_filter
        self.prior_rate_mtx_zerodiag = prior_rate_mtx_zerodiag

        self._interpolants = self.init_interpolants()

        _interpolants = self._interpolants.reshape((-1, self._interpolants.shape[-1]), order='F')
        self.interpolant_fn = interpolate.interp1d(self.time_grid, _interpolants, kind='previous', bounds_error=False,
                                                   fill_value=(_interpolants[:, 0], _interpolants[:, -1]))

    @property
    def interpolants(self):
        return self._interpolants

    def init_interpolants(self):
        # Get the belief interpolants
        p_f = np.asarray(self.belief_filter(self.time_grid))  # Dimension Z x Z' x T
        A = self.prior_rate_mtx_zerodiag  # Dimension ZxZ'

        # compute posterior mtx entries at these interpolant positions
        interpolants = p_f * A[:, :, None]  # (p_f[None, :, :] * A[:, :, None]) / p_f[:, None, :]
        diag = np.einsum('iit->it', interpolants)
        diag[:] = -1 * np.sum(interpolants, axis=1)

        if self.clip_above is not None:
            return np.clip(interpolants, a_min=None, a_max=self.clip_above)
        else:
            return interpolants

    def __call__(self, t: Any):
        """
         Evaluates the rate matrix according to #TODO

        :param t: time point(s) for evaluation can be float, singleton or array of size n_times
        :return: [n_states x n_states] or [n_states x n_states x n_time_points] posterior rate matrix
        """

        # Cast as numpy
        t = np.asarray(t)

        # interpolate the array
        res = self.interpolant_fn(t)

        if t.ndim == 0:
            res = res.reshape(self._interpolants.shape[:-1], order='F')
        else:
            res = res.reshape((*self._interpolants.shape[:-1], *t.shape), order='F')
        return res
