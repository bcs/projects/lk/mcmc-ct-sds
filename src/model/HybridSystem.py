"""
Module holding a class used for (forward) simulation of a continuous-discrete Hybrid system.
"""
import numpy as np
import torch
from torch import nn
from joblib import Parallel, delayed
from typing import List, Union, Tuple, Any


class HybridSystem(nn.Module):

    def __init__(self, rate_mtx: Union[np.ndarray, torch.Tensor],
                 initial_dist_mjp: Union[np.ndarray, torch.Tensor],
                 initial_sde_mean: Union[np.ndarray, torch.Tensor],
                 initial_sde_cov: Union[np.ndarray, torch.Tensor],
                 diffusion_drift_slope: Union[np.ndarray, torch.Tensor],
                 diffusion_drift_intercept: Union[np.ndarray, torch.Tensor],
                 diffusion_cov: Union[np.ndarray, torch.Tensor],
                 obs_cov: Union[np.ndarray, torch.Tensor]
                 ):

        super().__init__()

        # Register MJP parameters
        self.n_states = None
        self.register_buffer('_rate_mtx', None)
        self.register_buffer('rate_mtx_zerodiag', None)
        self.register_buffer('initial_dist_mjp', initial_dist_mjp)
        self.posterior_rate_mtx = None

        # Register SDE parameters
        self.n_dim = None
        self.register_buffer('diffusion_drift_slope', None)
        self.register_buffer('_diffusion_cov_chol', None)
        self.register_buffer('_obs_cov_chol', None)
        self.register_buffer('_obs_cov_inv', None)
        self.register_buffer('initial_sde_mean', None)
        self.register_buffer('initial_sde_cov', None)

        # Register result parameters
        self.jump_times = None
        self.mjp_paths = None
        self.mjp_paths_discretized = None
        self.time_grid = None
        self.sde_paths = None

        # Store input values
        self.rate_mtx = rate_mtx
        self.initial_dist_mjp = initial_dist_mjp
        self.initial_sde_mean = torch.as_tensor(initial_sde_mean, dtype=torch.float64)
        self.initial_sde_cov = torch.as_tensor(initial_sde_cov, dtype=torch.float64)
        self.diffusion_drift = torch.as_tensor(diffusion_drift_slope, dtype=torch.float64)
        self.diffusion_intercept = torch.as_tensor(diffusion_drift_intercept, dtype=torch.float64)
        self.diffusion_cov = torch.as_tensor(diffusion_cov, dtype=torch.float64)
        self.obs_cov = obs_cov

    @property
    def rate_mtx(self):
        return self._rate_mtx

    @rate_mtx.setter
    def rate_mtx(self, val):
        val = torch.as_tensor(val, dtype=torch.float64)
        assert val.ndim == 2, 'Required input format for Q matrix is [n_states, n_states]'
        assert val.shape[0] == val.shape[1], 'Q matrix must be symmetric'
        assert torch.isclose(val.sum(1),
                             torch.zeros(val.shape[0], dtype=torch.float64)).all(), 'Q matrix has fulfil row-sum-zero'
        self.n_states = val.shape[0]
        self._rate_mtx = val
        self.rate_mtx_zerodiag = val - torch.diag(torch.diag(val))

    @property
    def diffusion_cov_chol(self):
        return self._diffusion_cov_chol

    @property
    def diffusion_cov_inv(self):
        return self._diffusion_cov_inv

    @property
    def diffusion_cov(self):
        L = torch.tril(self._diffusion_cov_chol)
        L_T = L.transpose(-1, -2)
        return L @ L_T

    @diffusion_cov.setter
    def diffusion_cov(self, val):
        assert val.ndim == 3, 'Required input format for diffusion_cov is [n_states, n_dim, n_dim]'
        self.n_dim = val.shape[1]
        L = torch.cholesky(val)
        self._diffusion_cov_chol = L
        self._diffusion_cov_inv = torch.inverse(L @ L.transpose(-1, -2))

    @property
    def obs_cov(self):
        L = torch.tril(self.obs_cov_chol)
        L_T = L.transpose(-1, -2)
        return L @ L_T

    @property
    def obs_cov_chol(self):
        return self._obs_cov_chol

    @obs_cov_chol.setter
    def obs_cov_chol(self, val):
        if self._obs_cov_chol is None:
            self._obs_cov_chol = nn.Parameter(val)
        else:
            self._obs_cov_chol.data = val
        self._obs_cov_inv = torch.inverse(self._obs_cov_chol @ self._obs_cov_chol.transpose(-1, -2))

    @obs_cov.setter
    def obs_cov(self, val):
        assert val.ndim == 2, 'Required input format for obs_cov is [n_dim, n_dim]'
        L = torch.cholesky(val)
        self.obs_cov_chol = L

    def sample(self,
               t_span: Union[list, np.ndarray, torch.Tensor],
               n_trajectories: int = 1,
               dt: float = 1e-3,
               n_jobs: int = 1,
               t_obs = None
               ):
        """
        Return a Trajectory object holding sampled trajectories from prior or posterior.

        :param t_span: [2,] array-like, the time interval to be simulated
        :param n_trajectories: Int specifying the number of samples.
        :param dt: Float, the time step used for Euler-Maruyama.
        :param n_jobs: Int, number of parallel jobs (-> joblib.Parallel)
        :param t_obs: If given, synthetic observations at these time points will be generated
        :return: time grid, simulated SDE paths (on this time grid), simulated MJP paths (on this time grid),
                  MJP jump times, MJP paths
        """
        jump_times, mjp_paths, mjp_paths_discretized, time_grid = self.sample_mjp(t_span, n_trajectories, dt=dt,
                                                                                  n_jobs=n_jobs)
        time_grid, sde_paths = self.sample_conditional_diffusion(n_trajectories, mjp_paths_discretized, t_span, dt,
                                                                 n_jobs)

        if t_obs is not None:
            observations = self.sample_observations(time_grid, sde_paths, t_obs)
            return time_grid, sde_paths, mjp_paths_discretized, jump_times, mjp_paths, observations
        else:
            return time_grid, sde_paths, mjp_paths_discretized, jump_times, mjp_paths

    # ------------------- Generate noisy observations ------------
    def sample_observations(self, time_grid, sde_paths, observation_times):
        n_samples = sde_paths.shape[0]

        observations = []
        for n in range(n_samples):
            time_grid_indices = np.searchsorted(time_grid, observation_times[n])
            y = np.moveaxis(sde_paths[n, :, time_grid_indices], 1, 0)
            observations.append(self._observe(y))

        return observations

    def _observe(self, y: np.ndarray) -> np.ndarray:
        """
        Return noisy observations for latent Y-values y.

        :param y: [n_dim, n_observations] array, values of latent Y-process at observation time points
        :return: np.ndarray
        """
        if self.n_dim == 1:
            observations = np.random.normal(loc=y, scale=np.sqrt(self.obs_cov.detach()))
        else:
            res = []
            n_samples = y.shape[-1]
            # TODO parallelize
            for i in range(n_samples):
                res.append(np.random.multivariate_normal(y[:, i], self.obs_cov.detach()))
            observations = np.array(res)

        return observations

    # ----------- Gillespie sampling of discrete-valued trajectories
    def sample_mjp(self,
                   t_span: Union[list, np.ndarray, torch.Tensor],
                   n_trajectories: int,
                   dt: float, n_jobs: int = 1) -> Tuple[List, List, Any, Any]:
        """
        Gillespie sampling.
        # TODO update doc

        :param t_span: [2,] array-like, the time interval to be simulated
        :param n_trajectories: Integer specifying the number of trajectories to be drawn
        :param dt: float, time discretization
        :param n_jobs: int, number of parallel jobs
        :return: tuple, (trajectories, jump_times)
        """
        state_trajectories = []
        jump_times = []

        res = Parallel(n_jobs=n_jobs)(delayed(self._sample_mjp)(t_span) for i in range(n_trajectories))

        for elem in res:
            jump_times.append(elem[0])
            state_trajectories.append(elem[1])

        state_trajectories_discretized, time_grid = self.discretize_mjp_trajectories(jump_times, state_trajectories,
                                                                                     t_span, dt)

        return jump_times, state_trajectories, state_trajectories_discretized, time_grid

    # TODO doc
    def _sample_mjp(self,
                    t_span: Union[list, np.ndarray, torch.Tensor],
                    ) -> Tuple[List, List]:
        trajectory = []
        jump_times = []
        t_start, t_end = t_span

        z_init = np.random.choice(np.arange(self.n_states), p=self.initial_dist_mjp)
        trajectory.append(z_init)
        t_cur = t_start
        while t_cur < t_end:
            z_cur = trajectory[-1]
            Q_xy = self.rate_mtx_zerodiag[z_cur].numpy()
            Q_xx = -1 * np.sum(Q_xy)

            sojourn_time = np.random.exponential(
                -1 * 1 / Q_xx)  # Note: the numpy version uses shape parameterization
            t_next = t_cur + sojourn_time
            z_next = np.random.choice(np.arange(self.n_states), p=-1 * Q_xy / Q_xx)

            if t_next <= t_end:
                trajectory.append(z_next)
                jump_times.append(t_next)
            t_cur = t_next

        return jump_times, trajectory

    @staticmethod
    def discretize_mjp_trajectories(jump_times, state_trajectories, t_span, dt):
        n_trajectories = len(state_trajectories)

        timesteps = np.arange(t_span[0], t_span[1] + dt, dt)
        n_timesteps = timesteps.shape[0]
        state_paths_discretized = np.zeros((n_trajectories, n_timesteps), dtype=int)
        for n in range(n_trajectories):
            insertion_indices = np.searchsorted(timesteps, jump_times[n])
            indices = np.concatenate((np.array([0]), insertion_indices, np.array([len(timesteps) - 1])))
            for pair_idx, pair in enumerate(zip(indices[:-1], indices[1:])):
                state_paths_discretized[n, pair[0]:pair[1]] = int(state_trajectories[n][pair_idx])

        return state_paths_discretized, timesteps

    # ----------- Euler-Maruyama sampling of continuous-valued trajectories
    # Todo doc
    def sample_conditional_diffusion(self, n_trajectories: int, discrete_trajectories: np.ndarray,
                                     t_span: Union[list, np.ndarray, torch.Tensor],
                                     dt: float,
                                     n_jobs: int = 1):
        time = np.arange(t_span[0], t_span[1] + dt, dt)

        res = Parallel(n_jobs=n_jobs)(
            delayed(self._conditional_euler_maruyama)(discrete_trajectories[n], time, dt) for n in
            range(n_trajectories))

        return time, np.array(res)

    # TODO doc
    def _conditional_euler_maruyama(self, discrete_trajectory: np.ndarray, time: np.ndarray, dt: float):
        continuous_trajectory = np.zeros((self.n_dim, len(discrete_trajectory)))

        z_0 = discrete_trajectory[0]
        y_0 = np.random.multivariate_normal(self.initial_sde_mean,
                                            self.initial_sde_cov)
        drift = self._drift
        dispersion = self._dispersion

        continuous_trajectory[:, 0] = y_0

        if self.n_dim == 1:
            dW = np.random.normal(scale=np.sqrt(dt), size=(len(time)))
            dW = dW[:, None]
        else:
            dW = np.random.multivariate_normal(np.zeros(self.n_dim), dt * np.eye(self.n_dim), size=(len(time)))

        for i in range(1, len(time)):
            t_cur = time[i - 1]
            z_cur = discrete_trajectory[i - 1]
            y_cur = continuous_trajectory[:, i - 1]
            y_next = (y_cur + drift(y_cur, z_cur, t_cur) * dt + dispersion(z_cur, t_cur) @ dW[i])
            continuous_trajectory[:, i] = y_next

        return continuous_trajectory

    def _drift(self, y, z, t):
        return self.diffusion_drift[z].numpy() @ y + self.diffusion_intercept[z].numpy()

    def _dispersion(self, z, t):
        return self.diffusion_cov_chol[z].detach().numpy()