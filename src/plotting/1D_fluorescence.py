import matplotlib.gridspec as gridspec
import numpy as np
import matplotlib
import matplotlib.pyplot as plt
import os
import pickle
import scipy.stats as st
import torch
from src.utils.paths import DATA_PATH, CHECKPOINT_PATH, PLOTS_PATH

matplotlib.rcParams['axes.linewidth'] = 1.5 # set the value globally
matplotlib.rcParams['mathtext.fontset'] = 'cm'
matplotlib.rcParams['mathtext.rm'] = 'serif'

# -------------------- Loading --------------------
# Get experiment name (file names have to match)
experiment_name = __file__[__file__.rfind(
        '/') + 1: -3]  # extract the filename between last `/' and the `.npy', i.e., the experiment's name

# Load data
data = np.load('../../' + DATA_PATH + '1D_fluorescence/trace_5.npy')
t_obs = data[:, 0]
X_obs = data[:, 1]
X_obs = X_obs[None, :]
time_scaling = 10./t_obs[-1]
t_obs *= time_scaling
#t_span = [0.0, 0.4]
t_span = [0.0, 10.] # Scale for numerical stability

t_obs = t_obs[1:-1]  # throw away observations at boundaries
empirical_mean = np.mean(X_obs[0, :])
empirical_std = np.std(X_obs[0, :])
X_obs = (X_obs[:, 1:-1] - empirical_mean)/empirical_std  # rescale y-axis to prevent numerical issues
data = X_obs

# Create plots dir
if not os.path.exists('../../' + PLOTS_PATH + experiment_name):
    os.makedirs('../../' + PLOTS_PATH + experiment_name)

# Load PHS object
#timestamp = '20220122-175330'
#timestamp = '20220123-132535'
timestamp = '20220127-091859'
PHS = torch.load('../../' + CHECKPOINT_PATH + f'shs_fit_1D_fluorescence_{timestamp}_iter_final.pt')
# --------------------------------------------------------------------------------
# Plotting

def plot_overview():

    heights = [1, 3]
    fig = plt.figure(figsize=(5, 3.5))

    gs = gridspec.GridSpec(ncols=1, nrows=2, figure=fig, height_ratios=heights, hspace=0.1)
    ax0 = fig.add_subplot(gs[0, 0])
    ax1 = fig.add_subplot(gs[1, 0])

    # Last 100
    mjp_paths = []
    sde_paths = []
    for i in range(50000, 100000, 5):
        path, disc_grid = PHS.discretize_mjp_trajectories([PHS.samples['mjp_jump_times'][i]], [PHS.samples['mjp_paths'][i]], PHS.t_span, 0.01)
        mjp_paths.append(path)
        sde_paths.append(PHS.samples['sde_paths'][i][0])
    sde_paths = np.array(sde_paths)
    mjp_paths = np.array(mjp_paths)
    hist_time = np.repeat(PHS.time_grid, sde_paths.shape[0])
    hvals, xedges, yedges = np.histogram2d(hist_time, sde_paths.flatten(order='F'), bins=[600, 100], range=[[0, 10.], [-2.1, 2.4]])
    hvals_norm = hvals/np.sum(hvals, axis=1)[:, None]
    ax1.matshow(hvals_norm.T, aspect='auto', origin='lower', vmax=hvals_norm.max(), extent=(0, 10, -2.1, 2.4))
    #ax3.pcolormesh(xedges, yedges, hvals_norm.T, cmap='viridis', vmax=hvals_norm.max())

    dist_empirical = np.mean(mjp_paths.squeeze(), axis=0)
    ax0.plot(disc_grid[:-1], dist_empirical[:-1], c='tab:blue', lw=2)

    # Plot controls (known from experimentalists)
    t_control = np.array([0, 59, 239, 479, 569, 990]) * time_scaling
    control = np.array([0, 1, 0, 1, 0, 0])
    ax0.step(t_control, control, where='post', ls='--', c='tab:orange', alpha=.8, lw=2)

    # Plot data
    ax1.scatter(t_obs, X_obs[0], marker='x', c='white', zorder=11, alpha=.6, lw=.6)
    # Time scale back-schmeckeling
    ax1.set_xticks(list(np.array([0, 200, 400, 600, 800]) * time_scaling) + [10.])
    ax1.set_xticklabels([0, 200, 400, 600, 800, 1000])

    # Style stuff
    ax0.set_xticklabels([])
    ax1.xaxis.set_ticks_position('bottom')
    ax0.set_ylabel('$p_Z$', fontsize='x-large')
    ax1.set_ylabel('Fluorescence [a.u.]', fontdict={'size': 'x-large', 'name': 'cmr10'})
    ax0.yaxis.set_label_coords(-0.07, 0.5)
    ax1.yaxis.set_label_coords(-0.07, 0.5)
    ax1.set_xlabel('Time [min]', fontdict={'size': 'x-large', 'name': 'cmr10'})


    plt.subplots_adjust(left=0.11, right=0.96, top=0.985, bottom=0.14)
    plt.savefig('../../' + PLOTS_PATH + experiment_name + f'/overview_{timestamp}.pdf', format='pdf')
    plt.close()


def plot_parameters():

    # Plot the parameter distributions
    widths = [1, 1]
    heights = [1, 1]
    fig = plt.figure(figsize=(4, 3.5))
    gs = gridspec.GridSpec(ncols=2, nrows=2, figure=fig, width_ratios=widths,
                           height_ratios=heights, wspace=0.06, hspace=0.35)
    ax0 = fig.add_subplot(gs[0, 0])
    ax1 = fig.add_subplot(gs[0, 1])
    #ax2 = fig.add_subplot(gs[0, 2])
    ax3 = fig.add_subplot(gs[1, :2])
    #ax6 = fig.add_subplot(gs[1, 1], sharex=ax0, sharey=ax2)
    #ax4 = fig.add_subplot(gs[1, 1])
    #ax5 = fig.add_subplot(gs[1, 2])
    #plt.setp(ax0.get_xticklabels(), visible=False)  # No x-ticklabels
    #plt.setp(ax2.get_xticklabels(), visible=False)  # No x-ticklabels
    #plt.setp(ax4.get_yticklabels(), visible=False)  # No y-ticklabels

    A = np.array(PHS.samples['diffusion_slope'][-50000::5])
    b = np.array(PHS.samples['diffusion_intercept'][-50000::5])
    Lambda = np.array(PHS.samples['rate_mtx'][-50000::5])
    Sigma = np.array(PHS.samples['obs_cov'][-50000::5])
    D = np.array(PHS.samples['diffusion_cov'][-50000::5])

    # Slopes in one plot
    ax0.hist(A[:, 0, :, :].ravel()/(time_scaling*empirical_std), alpha=.5, bins=20)
    ax0.hist(A[:, 1, :, :].ravel()/(time_scaling*empirical_std), alpha=.5, bins=20)
    ax0.set_xlabel('$\mathrm{I/min}$')

    # Intercepts in one plot
    ax3.hist(b[:, 0, :].ravel()*empirical_std + empirical_mean, bins=20, alpha=.5)
    ax3.hist(b[:, 1, :].ravel()*empirical_std + empirical_mean, bins=20, alpha=.5)
    ax3.set_xlabel('$\mathrm{I}$')
    #ax1.set_xlim((0.5, 2.0))
    #ax4.set_xlim((-2.0, -0.5))

    # Rates inside top middle
    ax1.hist(-1 * Lambda[:, 0, 0]/time_scaling.ravel(), alpha=.5, color='tab:blue', bins=60)
    ax1.hist(-1 * Lambda[:, 1, 1]/time_scaling.ravel(), alpha=.5, color='tab:orange', bins=60)
    # Ground truth
    #ax1.set_xlim((0., 3.))
    ax1.set_xlabel('$\mathrm{1/min}$')

    #All y-ticks and -labels invisible
    ax0.set_yticks([])
    ax1.set_yticks([])
    ax3.set_yticks([])

    plt.subplots_adjust(left=0.01, right=0.97, top=0.99, bottom=0.13)
    plt.savefig('../../' + PLOTS_PATH + experiment_name + f'/parameter_histograms_{timestamp}.pdf', format='pdf')
    plt.close()

plot_parameters()
plot_overview()