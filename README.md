# Markov Chain Monte Carlo for Continuous-Time Switching Dynamical Systems

Code for the ICML 2022 publication "Markov Chain Monte Carlo for Continuous-Time Switching Dynamical Systems" by Lukas Köhs, Bastian Alt, and Heinz Koeppl.

### Installation

To install required packages, run

```setup
pip install -r requirements.txt
```

Code has been developed and tested using python 3.6.8.

### Project structure

```/exp```
Experiment files. Correspondence to figures is as follows:
- Fig 3: 1D_synSSDE_50obs_neqspaced_0-05_slow_relax_MH.py
- Fig 4: 1D_fluorescence_MH.py

```/src```
Source files for the model and plotting.

```/data```
Holds gene expression measurement data, `trace_5.npy'.
